/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.mapper;

import net.d2dcloud.entity.Permission;
import net.d2dcloud.core.tk.mapper.BaseMapper;

/**
 *
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
public interface PermissionMapper extends BaseMapper<Permission> {

}