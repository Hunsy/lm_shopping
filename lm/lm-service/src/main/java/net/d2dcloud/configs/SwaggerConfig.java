package net.d2dcloud.configs;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.request.async.DeferredResult;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.List;

/**
 * swagger配置
 *
 * @author liuzw
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {


  /**
   * @return
   * @author liuzw
   * @description adminApi
   * @date 2019/4/17 18:17
   **/
  @Bean
  public Docket adminApi() {


    return new Docket (DocumentationType.SWAGGER_2)
      .groupName ("lm")
      .genericModelSubstitutes (DeferredResult.class)
//                .genericModelSubstitutes(ResponseEntity.class)
      .useDefaultResponseMessages (false)
      .forCodeGeneration (true)
      //最终调用接口后会和paths拼接在一起
      .pathMapping ("/")
      .select ()
      //过滤的接口
//                .paths(Predicates.or(regex("/.*")))
      .build ()
      .globalOperationParameters (getParis ())
      .apiInfo (adminMetaData ());

  }

  private ApiInfo adminMetaData() {
    return new ApiInfoBuilder ()
      .title ("lm API文档")
      .description ("lm")
      .termsOfServiceUrl ("https://api.d2dcloud.com")
      .version ("2.0.0")
      .build ();
  }


  private List<Parameter> getParis() {
    List<Parameter> pars = new ArrayList<> ();
    pars.add (new ParameterBuilder ().name ("Authorization")
      .description ("token")
      .modelRef (new ModelRef ("string"))
      .parameterType ("header")
      .defaultValue ("Token ")
      .required (false).build ());
    return pars;
  }
}
