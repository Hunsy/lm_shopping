/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import tk.mybatis.spring.annotation.MapperScan;

/**
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@SpringBootApplication
@MapperScan(basePackages = {"net.d2dcloud.mapper"})
@ServletComponentScan
@EnableDiscoveryClient
@EnableFeignClients(basePackages = {"net.d2dcloud.controller"})
public class LmApplication {

  /**
   * 程序入口
   */
  public static void main(String[] args) {
    SpringApplication.run (LmApplication.class, args);
  }

}
