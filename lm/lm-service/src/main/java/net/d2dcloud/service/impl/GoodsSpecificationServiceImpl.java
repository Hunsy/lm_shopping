/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.service.impl;

import net.d2dcloud.entity.GoodsSpecification;
import net.d2dcloud.core.tk.service.impl.BaseServiceImpl;
import net.d2dcloud.service.GoodsSpecificationService;

import org.springframework.stereotype.Service;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.util.Sqls;

import java.util.List;

/**
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@Service
public class GoodsSpecificationServiceImpl extends BaseServiceImpl<GoodsSpecification> implements GoodsSpecificationService {


  @Override
  public GoodsSpecification selectById(Object id) {
    GoodsSpecification goodsSpecification = new GoodsSpecification();
    goodsSpecification.setId(id.toString());
    goodsSpecification.setDeleted(false);
    return selectOne(goodsSpecification);
  }

  @Override
  public Integer deleteById(String id) {
    GoodsSpecification goodsSpecification = new GoodsSpecification();
    goodsSpecification.setDeleted(true);
    Example ex = Example.builder(GoodsSpecification.class)
        .where(Sqls.custom()
            .andEqualTo("id", id))
        .build();
    return super.updatedByExampleSelective(goodsSpecification, ex);
  }

  @Override
  public List<GoodsSpecification> selectByIds(List<String> ids) {
    Example ex = Example.builder(GoodsSpecification.class)
        .where(Sqls.custom()
            .andIn("id", ids)
            .andEqualTo("deleted", false))
        .build();
    return super.selectByExample(ex);
  }
}