/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.service.impl;

import net.d2dcloud.entity.Permission;
import net.d2dcloud.core.tk.service.impl.BaseServiceImpl;
import net.d2dcloud.service.PermissionService;

import org.springframework.stereotype.Service;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.util.Sqls;

import java.util.List;

/**
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@Service
public class PermissionServiceImpl extends BaseServiceImpl<Permission> implements PermissionService {


  @Override
  public Permission selectById(Object id) {
    Permission permission = new Permission();
    permission.setId(id.toString());
    permission.setDeleted(false);
    return selectOne(permission);
  }

  @Override
  public Integer deleteById(String id) {
    Permission permission = new Permission();
    permission.setDeleted(true);
    Example ex = Example.builder(Permission.class)
        .where(Sqls.custom()
            .andEqualTo("id", id))
        .build();
    return super.updatedByExampleSelective(permission, ex);
  }

  @Override
  public List<Permission> selectByIds(List<String> ids) {
    Example ex = Example.builder(Permission.class)
        .where(Sqls.custom()
            .andIn("id", ids)
            .andEqualTo("deleted", false))
        .build();
    return super.selectByExample(ex);
  }
}