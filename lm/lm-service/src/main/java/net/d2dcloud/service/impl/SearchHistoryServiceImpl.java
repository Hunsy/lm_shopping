/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.service.impl;

import net.d2dcloud.entity.SearchHistory;
import net.d2dcloud.core.tk.service.impl.BaseServiceImpl;
import net.d2dcloud.service.SearchHistoryService;

import org.springframework.stereotype.Service;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.util.Sqls;

import java.util.List;

/**
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@Service
public class SearchHistoryServiceImpl extends BaseServiceImpl<SearchHistory> implements SearchHistoryService {


  @Override
  public SearchHistory selectById(Object id) {
    SearchHistory searchHistory = new SearchHistory();
    searchHistory.setId(id.toString());
    searchHistory.setDeleted(false);
    return selectOne(searchHistory);
  }

  @Override
  public Integer deleteById(String id) {
    SearchHistory searchHistory = new SearchHistory();
    searchHistory.setDeleted(true);
    Example ex = Example.builder(SearchHistory.class)
        .where(Sqls.custom()
            .andEqualTo("id", id))
        .build();
    return super.updatedByExampleSelective(searchHistory, ex);
  }

  @Override
  public List<SearchHistory> selectByIds(List<String> ids) {
    Example ex = Example.builder(SearchHistory.class)
        .where(Sqls.custom()
            .andIn("id", ids)
            .andEqualTo("deleted", false))
        .build();
    return super.selectByExample(ex);
  }
}