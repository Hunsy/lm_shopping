/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.service.impl;

import net.d2dcloud.entity.Keyword;
import net.d2dcloud.core.tk.service.impl.BaseServiceImpl;
import net.d2dcloud.service.KeywordService;

import org.springframework.stereotype.Service;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.util.Sqls;

import java.util.List;

/**
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@Service
public class KeywordServiceImpl extends BaseServiceImpl<Keyword> implements KeywordService {


  @Override
  public Keyword selectById(Object id) {
    Keyword keyword = new Keyword();
    keyword.setId(id.toString());
    keyword.setDeleted(false);
    return selectOne(keyword);
  }

  @Override
  public Integer deleteById(String id) {
    Keyword keyword = new Keyword();
    keyword.setDeleted(true);
    Example ex = Example.builder(Keyword.class)
        .where(Sqls.custom()
            .andEqualTo("id", id))
        .build();
    return super.updatedByExampleSelective(keyword, ex);
  }

  @Override
  public List<Keyword> selectByIds(List<String> ids) {
    Example ex = Example.builder(Keyword.class)
        .where(Sqls.custom()
            .andIn("id", ids)
            .andEqualTo("deleted", false))
        .build();
    return super.selectByExample(ex);
  }
}