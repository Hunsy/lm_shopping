/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.service.impl;

import net.d2dcloud.entity.User;
import net.d2dcloud.core.tk.service.impl.BaseServiceImpl;
import net.d2dcloud.service.UserService;

import org.springframework.stereotype.Service;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.util.Sqls;

import java.util.List;

/**
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@Service
public class UserServiceImpl extends BaseServiceImpl<User> implements UserService {


  @Override
  public User selectById(Object id) {
    User user = new User();
    user.setId(id.toString());
    user.setDeleted(false);
    return selectOne(user);
  }

  @Override
  public Integer deleteById(String id) {
    User user = new User();
    user.setDeleted(true);
    Example ex = Example.builder(User.class)
        .where(Sqls.custom()
            .andEqualTo("id", id))
        .build();
    return super.updatedByExampleSelective(user, ex);
  }

  @Override
  public List<User> selectByIds(List<String> ids) {
    Example ex = Example.builder(User.class)
        .where(Sqls.custom()
            .andIn("id", ids)
            .andEqualTo("deleted", false))
        .build();
    return super.selectByExample(ex);
  }
}