/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.service.impl;

import net.d2dcloud.entity.Order;
import net.d2dcloud.core.tk.service.impl.BaseServiceImpl;
import net.d2dcloud.service.OrderService;

import org.springframework.stereotype.Service;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.util.Sqls;

import java.util.List;

/**
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@Service
public class OrderServiceImpl extends BaseServiceImpl<Order> implements OrderService {


  @Override
  public Order selectById(Object id) {
    Order order = new Order();
    order.setId(id.toString());
    order.setDeleted(false);
    return selectOne(order);
  }

  @Override
  public Integer deleteById(String id) {
    Order order = new Order();
    order.setDeleted(true);
    Example ex = Example.builder(Order.class)
        .where(Sqls.custom()
            .andEqualTo("id", id))
        .build();
    return super.updatedByExampleSelective(order, ex);
  }

  @Override
  public List<Order> selectByIds(List<String> ids) {
    Example ex = Example.builder(Order.class)
        .where(Sqls.custom()
            .andIn("id", ids)
            .andEqualTo("deleted", false))
        .build();
    return super.selectByExample(ex);
  }
}