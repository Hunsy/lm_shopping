/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.service.impl;

import net.d2dcloud.entity.Region;
import net.d2dcloud.core.tk.service.impl.BaseServiceImpl;
import net.d2dcloud.service.RegionService;

import org.springframework.stereotype.Service;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.util.Sqls;

import java.util.List;

/**
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@Service
public class RegionServiceImpl extends BaseServiceImpl<Region> implements RegionService {


  @Override
  public Region selectById(Object id) {
    Region region = new Region();
    region.setId(id.toString());
    return selectOne(region);
  }


  @Override
  public List<Region> selectByIds(List<String> ids) {
    Example ex = Example.builder(Region.class)
        .where(Sqls.custom()
            .andIn("id", ids))
        .build();
    return super.selectByExample(ex);
  }
}