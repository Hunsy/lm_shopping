/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.service.impl;

import net.d2dcloud.entity.Cart;
import net.d2dcloud.core.tk.service.impl.BaseServiceImpl;
import net.d2dcloud.service.CartService;

import org.springframework.stereotype.Service;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.util.Sqls;

import java.util.List;

/**
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@Service
public class CartServiceImpl extends BaseServiceImpl<Cart> implements CartService {


  @Override
  public Cart selectById(Object id) {
    Cart cart = new Cart();
    cart.setId(id.toString());
    cart.setDeleted(false);
    return selectOne(cart);
  }

  @Override
  public Integer deleteById(String id) {
    Cart cart = new Cart();
    cart.setDeleted(true);
    Example ex = Example.builder(Cart.class)
        .where(Sqls.custom()
            .andEqualTo("id", id))
        .build();
    return super.updatedByExampleSelective(cart, ex);
  }

  @Override
  public List<Cart> selectByIds(List<String> ids) {
    Example ex = Example.builder(Cart.class)
        .where(Sqls.custom()
            .andIn("id", ids)
            .andEqualTo("deleted", false))
        .build();
    return super.selectByExample(ex);
  }
}