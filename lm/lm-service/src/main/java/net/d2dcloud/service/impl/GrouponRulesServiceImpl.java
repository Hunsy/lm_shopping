/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.service.impl;

import net.d2dcloud.entity.GrouponRules;
import net.d2dcloud.core.tk.service.impl.BaseServiceImpl;
import net.d2dcloud.service.GrouponRulesService;

import org.springframework.stereotype.Service;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.util.Sqls;

import java.util.List;

/**
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@Service
public class GrouponRulesServiceImpl extends BaseServiceImpl<GrouponRules> implements GrouponRulesService {


  @Override
  public GrouponRules selectById(Object id) {
    GrouponRules grouponRules = new GrouponRules();
    grouponRules.setId(id.toString());
    grouponRules.setDeleted(false);
    return selectOne(grouponRules);
  }

  @Override
  public Integer deleteById(String id) {
    GrouponRules grouponRules = new GrouponRules();
    grouponRules.setDeleted(true);
    Example ex = Example.builder(GrouponRules.class)
        .where(Sqls.custom()
            .andEqualTo("id", id))
        .build();
    return super.updatedByExampleSelective(grouponRules, ex);
  }

  @Override
  public List<GrouponRules> selectByIds(List<String> ids) {
    Example ex = Example.builder(GrouponRules.class)
        .where(Sqls.custom()
            .andIn("id", ids)
            .andEqualTo("deleted", false))
        .build();
    return super.selectByExample(ex);
  }
}