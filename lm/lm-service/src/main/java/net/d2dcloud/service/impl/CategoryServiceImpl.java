/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.service.impl;

import net.d2dcloud.entity.Category;
import net.d2dcloud.core.tk.service.impl.BaseServiceImpl;
import net.d2dcloud.service.CategoryService;

import org.springframework.stereotype.Service;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.util.Sqls;

import java.util.List;

/**
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@Service
public class CategoryServiceImpl extends BaseServiceImpl<Category> implements CategoryService {


  @Override
  public Category selectById(Object id) {
    Category category = new Category();
    category.setId(id.toString());
    category.setDeleted(false);
    return selectOne(category);
  }

  @Override
  public Integer deleteById(String id) {
    Category category = new Category();
    category.setDeleted(true);
    Example ex = Example.builder(Category.class)
        .where(Sqls.custom()
            .andEqualTo("id", id))
        .build();
    return super.updatedByExampleSelective(category, ex);
  }

  @Override
  public List<Category> selectByIds(List<String> ids) {
    Example ex = Example.builder(Category.class)
        .where(Sqls.custom()
            .andIn("id", ids)
            .andEqualTo("deleted", false))
        .build();
    return super.selectByExample(ex);
  }
}