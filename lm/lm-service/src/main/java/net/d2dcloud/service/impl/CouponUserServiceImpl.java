/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.service.impl;

import net.d2dcloud.entity.CouponUser;
import net.d2dcloud.core.tk.service.impl.BaseServiceImpl;
import net.d2dcloud.service.CouponUserService;

import org.springframework.stereotype.Service;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.util.Sqls;

import java.util.List;

/**
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@Service
public class CouponUserServiceImpl extends BaseServiceImpl<CouponUser> implements CouponUserService {


  @Override
  public CouponUser selectById(Object id) {
    CouponUser couponUser = new CouponUser();
    couponUser.setId(id.toString());
    couponUser.setDeleted(false);
    return selectOne(couponUser);
  }

  @Override
  public Integer deleteById(String id) {
    CouponUser couponUser = new CouponUser();
    couponUser.setDeleted(true);
    Example ex = Example.builder(CouponUser.class)
        .where(Sqls.custom()
            .andEqualTo("id", id))
        .build();
    return super.updatedByExampleSelective(couponUser, ex);
  }

  @Override
  public List<CouponUser> selectByIds(List<String> ids) {
    Example ex = Example.builder(CouponUser.class)
        .where(Sqls.custom()
            .andIn("id", ids)
            .andEqualTo("deleted", false))
        .build();
    return super.selectByExample(ex);
  }
}