/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.service.impl;

import net.d2dcloud.entity.GoodsProduct;
import net.d2dcloud.core.tk.service.impl.BaseServiceImpl;
import net.d2dcloud.service.GoodsProductService;

import org.springframework.stereotype.Service;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.util.Sqls;

import java.util.List;

/**
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@Service
public class GoodsProductServiceImpl extends BaseServiceImpl<GoodsProduct> implements GoodsProductService {


  @Override
  public GoodsProduct selectById(Object id) {
    GoodsProduct goodsProduct = new GoodsProduct();
    goodsProduct.setId(id.toString());
    goodsProduct.setDeleted(false);
    return selectOne(goodsProduct);
  }

  @Override
  public Integer deleteById(String id) {
    GoodsProduct goodsProduct = new GoodsProduct();
    goodsProduct.setDeleted(true);
    Example ex = Example.builder(GoodsProduct.class)
        .where(Sqls.custom()
            .andEqualTo("id", id))
        .build();
    return super.updatedByExampleSelective(goodsProduct, ex);
  }

  @Override
  public List<GoodsProduct> selectByIds(List<String> ids) {
    Example ex = Example.builder(GoodsProduct.class)
        .where(Sqls.custom()
            .andIn("id", ids)
            .andEqualTo("deleted", false))
        .build();
    return super.selectByExample(ex);
  }
}