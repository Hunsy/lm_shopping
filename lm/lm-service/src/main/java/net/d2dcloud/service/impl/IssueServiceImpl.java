/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.service.impl;

import net.d2dcloud.entity.Issue;
import net.d2dcloud.core.tk.service.impl.BaseServiceImpl;
import net.d2dcloud.service.IssueService;

import org.springframework.stereotype.Service;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.util.Sqls;

import java.util.List;

/**
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@Service
public class IssueServiceImpl extends BaseServiceImpl<Issue> implements IssueService {


  @Override
  public Issue selectById(Object id) {
    Issue issue = new Issue();
    issue.setId(id.toString());
    issue.setDeleted(false);
    return selectOne(issue);
  }

  @Override
  public Integer deleteById(String id) {
    Issue issue = new Issue();
    issue.setDeleted(true);
    Example ex = Example.builder(Issue.class)
        .where(Sqls.custom()
            .andEqualTo("id", id))
        .build();
    return super.updatedByExampleSelective(issue, ex);
  }

  @Override
  public List<Issue> selectByIds(List<String> ids) {
    Example ex = Example.builder(Issue.class)
        .where(Sqls.custom()
            .andIn("id", ids)
            .andEqualTo("deleted", false))
        .build();
    return super.selectByExample(ex);
  }
}