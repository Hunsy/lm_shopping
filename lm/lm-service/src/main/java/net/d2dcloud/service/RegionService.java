/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.service;

import net.d2dcloud.entity.Region;
import net.d2dcloud.core.tk.service.BaseService;

import java.util.List;

/**
 *
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
public interface RegionService extends BaseService<Region> {


  /**
   * 获取Region列表
   *
   * @param ids
   * @return
   */
  List<Region> selectByIds(List<String> ids);
}