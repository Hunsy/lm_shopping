/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.service.impl;

import net.d2dcloud.entity.Log;
import net.d2dcloud.core.tk.service.impl.BaseServiceImpl;
import net.d2dcloud.service.LogService;

import org.springframework.stereotype.Service;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.util.Sqls;

import java.util.List;

/**
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@Service
public class LogServiceImpl extends BaseServiceImpl<Log> implements LogService {


  @Override
  public Log selectById(Object id) {
    Log log = new Log();
    log.setId(id.toString());
    log.setDeleted(false);
    return selectOne(log);
  }

  @Override
  public Integer deleteById(String id) {
    Log log = new Log();
    log.setDeleted(true);
    Example ex = Example.builder(Log.class)
        .where(Sqls.custom()
            .andEqualTo("id", id))
        .build();
    return super.updatedByExampleSelective(log, ex);
  }

  @Override
  public List<Log> selectByIds(List<String> ids) {
    Example ex = Example.builder(Log.class)
        .where(Sqls.custom()
            .andIn("id", ids)
            .andEqualTo("deleted", false))
        .build();
    return super.selectByExample(ex);
  }
}