/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.service;

import net.d2dcloud.entity.GrouponRules;
import net.d2dcloud.core.tk.service.BaseService;

import java.util.List;

/**
 *
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
public interface GrouponRulesService extends BaseService<GrouponRules> {


  /**
   * 获取GrouponRules列表
   *
   * @param ids
   * @return
   */
  List<GrouponRules> selectByIds(List<String> ids);
}