/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.service.impl;

import net.d2dcloud.entity.OrderGoods;
import net.d2dcloud.core.tk.service.impl.BaseServiceImpl;
import net.d2dcloud.service.OrderGoodsService;

import org.springframework.stereotype.Service;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.util.Sqls;

import java.util.List;

/**
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@Service
public class OrderGoodsServiceImpl extends BaseServiceImpl<OrderGoods> implements OrderGoodsService {


  @Override
  public OrderGoods selectById(Object id) {
    OrderGoods orderGoods = new OrderGoods();
    orderGoods.setId(id.toString());
    orderGoods.setDeleted(false);
    return selectOne(orderGoods);
  }

  @Override
  public Integer deleteById(String id) {
    OrderGoods orderGoods = new OrderGoods();
    orderGoods.setDeleted(true);
    Example ex = Example.builder(OrderGoods.class)
        .where(Sqls.custom()
            .andEqualTo("id", id))
        .build();
    return super.updatedByExampleSelective(orderGoods, ex);
  }

  @Override
  public List<OrderGoods> selectByIds(List<String> ids) {
    Example ex = Example.builder(OrderGoods.class)
        .where(Sqls.custom()
            .andIn("id", ids)
            .andEqualTo("deleted", false))
        .build();
    return super.selectByExample(ex);
  }
}