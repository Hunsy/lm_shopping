/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.service.impl;

import net.d2dcloud.entity.Footprint;
import net.d2dcloud.core.tk.service.impl.BaseServiceImpl;
import net.d2dcloud.service.FootprintService;

import org.springframework.stereotype.Service;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.util.Sqls;

import java.util.List;

/**
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@Service
public class FootprintServiceImpl extends BaseServiceImpl<Footprint> implements FootprintService {


  @Override
  public Footprint selectById(Object id) {
    Footprint footprint = new Footprint();
    footprint.setId(id.toString());
    footprint.setDeleted(false);
    return selectOne(footprint);
  }

  @Override
  public Integer deleteById(String id) {
    Footprint footprint = new Footprint();
    footprint.setDeleted(true);
    Example ex = Example.builder(Footprint.class)
        .where(Sqls.custom()
            .andEqualTo("id", id))
        .build();
    return super.updatedByExampleSelective(footprint, ex);
  }

  @Override
  public List<Footprint> selectByIds(List<String> ids) {
    Example ex = Example.builder(Footprint.class)
        .where(Sqls.custom()
            .andIn("id", ids)
            .andEqualTo("deleted", false))
        .build();
    return super.selectByExample(ex);
  }
}