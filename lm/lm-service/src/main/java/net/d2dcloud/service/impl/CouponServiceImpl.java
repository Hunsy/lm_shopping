/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.service.impl;

import net.d2dcloud.entity.Coupon;
import net.d2dcloud.core.tk.service.impl.BaseServiceImpl;
import net.d2dcloud.service.CouponService;

import org.springframework.stereotype.Service;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.util.Sqls;

import java.util.List;

/**
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@Service
public class CouponServiceImpl extends BaseServiceImpl<Coupon> implements CouponService {


  @Override
  public Coupon selectById(Object id) {
    Coupon coupon = new Coupon();
    coupon.setId(id.toString());
    coupon.setDeleted(false);
    return selectOne(coupon);
  }

  @Override
  public Integer deleteById(String id) {
    Coupon coupon = new Coupon();
    coupon.setDeleted(true);
    Example ex = Example.builder(Coupon.class)
        .where(Sqls.custom()
            .andEqualTo("id", id))
        .build();
    return super.updatedByExampleSelective(coupon, ex);
  }

  @Override
  public List<Coupon> selectByIds(List<String> ids) {
    Example ex = Example.builder(Coupon.class)
        .where(Sqls.custom()
            .andIn("id", ids)
            .andEqualTo("deleted", false))
        .build();
    return super.selectByExample(ex);
  }
}