/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.service.impl;

import net.d2dcloud.entity.Ad;
import net.d2dcloud.core.tk.service.impl.BaseServiceImpl;
import net.d2dcloud.service.AdService;

import org.springframework.stereotype.Service;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.util.Sqls;

import java.util.List;

/**
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@Service
public class AdServiceImpl extends BaseServiceImpl<Ad> implements AdService {


  @Override
  public Ad selectById(Object id) {
    Ad ad = new Ad();
    ad.setId(id.toString());
    ad.setDeleted(false);
    return selectOne(ad);
  }

  @Override
  public Integer deleteById(String id) {
    Ad ad = new Ad();
    ad.setDeleted(true);
    Example ex = Example.builder(Ad.class)
        .where(Sqls.custom()
            .andEqualTo("id", id))
        .build();
    return super.updatedByExampleSelective(ad, ex);
  }

  @Override
  public List<Ad> selectByIds(List<String> ids) {
    Example ex = Example.builder(Ad.class)
        .where(Sqls.custom()
            .andIn("id", ids)
            .andEqualTo("deleted", false))
        .build();
    return super.selectByExample(ex);
  }
}