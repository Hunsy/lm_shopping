/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.service.impl;

import net.d2dcloud.entity.Brand;
import net.d2dcloud.core.tk.service.impl.BaseServiceImpl;
import net.d2dcloud.service.BrandService;

import org.springframework.stereotype.Service;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.util.Sqls;

import java.util.List;

/**
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@Service
public class BrandServiceImpl extends BaseServiceImpl<Brand> implements BrandService {


  @Override
  public Brand selectById(Object id) {
    Brand brand = new Brand();
    brand.setId(id.toString());
    brand.setDeleted(false);
    return selectOne(brand);
  }

  @Override
  public Integer deleteById(String id) {
    Brand brand = new Brand();
    brand.setDeleted(true);
    Example ex = Example.builder(Brand.class)
        .where(Sqls.custom()
            .andEqualTo("id", id))
        .build();
    return super.updatedByExampleSelective(brand, ex);
  }

  @Override
  public List<Brand> selectByIds(List<String> ids) {
    Example ex = Example.builder(Brand.class)
        .where(Sqls.custom()
            .andIn("id", ids)
            .andEqualTo("deleted", false))
        .build();
    return super.selectByExample(ex);
  }
}