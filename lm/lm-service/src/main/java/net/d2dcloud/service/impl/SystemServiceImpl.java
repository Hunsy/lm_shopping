/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.service.impl;

import net.d2dcloud.entity.System;
import net.d2dcloud.core.tk.service.impl.BaseServiceImpl;
import net.d2dcloud.service.SystemService;

import org.springframework.stereotype.Service;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.util.Sqls;

import java.util.List;

/**
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@Service
public class SystemServiceImpl extends BaseServiceImpl<System> implements SystemService {


  @Override
  public System selectById(Object id) {
    System system = new System();
    system.setId(id.toString());
    system.setDeleted(false);
    return selectOne(system);
  }

  @Override
  public Integer deleteById(String id) {
    System system = new System();
    system.setDeleted(true);
    Example ex = Example.builder(System.class)
        .where(Sqls.custom()
            .andEqualTo("id", id))
        .build();
    return super.updatedByExampleSelective(system, ex);
  }

  @Override
  public List<System> selectByIds(List<String> ids) {
    Example ex = Example.builder(System.class)
        .where(Sqls.custom()
            .andIn("id", ids)
            .andEqualTo("deleted", false))
        .build();
    return super.selectByExample(ex);
  }
}