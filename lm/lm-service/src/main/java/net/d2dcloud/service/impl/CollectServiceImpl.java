/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.service.impl;

import net.d2dcloud.entity.Collect;
import net.d2dcloud.core.tk.service.impl.BaseServiceImpl;
import net.d2dcloud.service.CollectService;

import org.springframework.stereotype.Service;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.util.Sqls;

import java.util.List;

/**
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@Service
public class CollectServiceImpl extends BaseServiceImpl<Collect> implements CollectService {


  @Override
  public Collect selectById(Object id) {
    Collect collect = new Collect();
    collect.setId(id.toString());
    collect.setDeleted(false);
    return selectOne(collect);
  }

  @Override
  public Integer deleteById(String id) {
    Collect collect = new Collect();
    collect.setDeleted(true);
    Example ex = Example.builder(Collect.class)
        .where(Sqls.custom()
            .andEqualTo("id", id))
        .build();
    return super.updatedByExampleSelective(collect, ex);
  }

  @Override
  public List<Collect> selectByIds(List<String> ids) {
    Example ex = Example.builder(Collect.class)
        .where(Sqls.custom()
            .andIn("id", ids)
            .andEqualTo("deleted", false))
        .build();
    return super.selectByExample(ex);
  }
}