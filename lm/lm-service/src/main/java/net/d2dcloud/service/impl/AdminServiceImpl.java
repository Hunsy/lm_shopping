/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.service.impl;

import net.d2dcloud.entity.Admin;
import net.d2dcloud.core.tk.service.impl.BaseServiceImpl;
import net.d2dcloud.service.AdminService;

import org.springframework.stereotype.Service;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.util.Sqls;

import java.util.List;

/**
 *
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@Service
public class  AdminServiceImpl extends BaseServiceImpl<Admin> implements AdminService {


  @Override
  public Admin selectById(Object id){
    Admin admin = new Admin();
    admin.setId(id.toString());
    admin.setDeleted (false);
    return selectOne(admin);
  }

  @Override
  public Integer deleteById(String id) {
    Admin admin = new Admin();
    admin.setDeleted (true);
    Example ex = Example.builder(Admin.class)
      .where(Sqls.custom()
        .andEqualTo("id", id))
      .build();
    return super.updatedByExampleSelective(admin, ex);
  }

  @Override
  public List<Admin> selectByIds(List<String> ids){
    Example ex = Example.builder(Admin.class)
    .where(Sqls.custom()
      .andIn("id", ids)
      .andEqualTo("deleted",false))
    .build();
    return super.selectByExample(ex);
  }
}