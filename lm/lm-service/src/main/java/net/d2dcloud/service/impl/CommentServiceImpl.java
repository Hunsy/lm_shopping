/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.service.impl;

import net.d2dcloud.entity.Comment;
import net.d2dcloud.core.tk.service.impl.BaseServiceImpl;
import net.d2dcloud.service.CommentService;

import org.springframework.stereotype.Service;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.util.Sqls;

import java.util.List;

/**
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@Service
public class CommentServiceImpl extends BaseServiceImpl<Comment> implements CommentService {


  @Override
  public Comment selectById(Object id) {
    Comment comment = new Comment();
    comment.setId(id.toString());
    comment.setDeleted(false);
    return selectOne(comment);
  }

  @Override
  public Integer deleteById(String id) {
    Comment comment = new Comment();
    comment.setDeleted(true);
    Example ex = Example.builder(Comment.class)
        .where(Sqls.custom()
            .andEqualTo("id", id))
        .build();
    return super.updatedByExampleSelective(comment, ex);
  }

  @Override
  public List<Comment> selectByIds(List<String> ids) {
    Example ex = Example.builder(Comment.class)
        .where(Sqls.custom()
            .andIn("id", ids)
            .andEqualTo("deleted", false))
        .build();
    return super.selectByExample(ex);
  }
}