/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.service.impl;

import net.d2dcloud.entity.Topic;
import net.d2dcloud.core.tk.service.impl.BaseServiceImpl;
import net.d2dcloud.service.TopicService;

import org.springframework.stereotype.Service;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.util.Sqls;

import java.util.List;

/**
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@Service
public class TopicServiceImpl extends BaseServiceImpl<Topic> implements TopicService {


  @Override
  public Topic selectById(Object id) {
    Topic topic = new Topic();
    topic.setId(id.toString());
    topic.setDeleted(false);
    return selectOne(topic);
  }

  @Override
  public Integer deleteById(String id) {
    Topic topic = new Topic();
    topic.setDeleted(true);
    Example ex = Example.builder(Topic.class)
        .where(Sqls.custom()
            .andEqualTo("id", id))
        .build();
    return super.updatedByExampleSelective(topic, ex);
  }

  @Override
  public List<Topic> selectByIds(List<String> ids) {
    Example ex = Example.builder(Topic.class)
        .where(Sqls.custom()
            .andIn("id", ids)
            .andEqualTo("deleted", false))
        .build();
    return super.selectByExample(ex);
  }
}