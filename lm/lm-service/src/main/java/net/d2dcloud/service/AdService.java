/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.service;

import net.d2dcloud.entity.Ad;
import net.d2dcloud.core.tk.service.BaseService;

import java.util.List;

/**
 *
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
public interface AdService extends BaseService<Ad> {


  /**
   * 获取Ad列表
   *
   * @param ids
   * @return
   */
  List<Ad> selectByIds(List<String> ids);
}