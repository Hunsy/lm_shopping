/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.service.impl;

import net.d2dcloud.entity.Notice;
import net.d2dcloud.core.tk.service.impl.BaseServiceImpl;
import net.d2dcloud.service.NoticeService;

import org.springframework.stereotype.Service;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.util.Sqls;

import java.util.List;

/**
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@Service
public class NoticeServiceImpl extends BaseServiceImpl<Notice> implements NoticeService {


  @Override
  public Notice selectById(Object id) {
    Notice notice = new Notice();
    notice.setId(id.toString());
    notice.setDeleted(false);
    return selectOne(notice);
  }

  @Override
  public Integer deleteById(String id) {
    Notice notice = new Notice();
    notice.setDeleted(true);
    Example ex = Example.builder(Notice.class)
        .where(Sqls.custom()
            .andEqualTo("id", id))
        .build();
    return super.updatedByExampleSelective(notice, ex);
  }

  @Override
  public List<Notice> selectByIds(List<String> ids) {
    Example ex = Example.builder(Notice.class)
        .where(Sqls.custom()
            .andIn("id", ids)
            .andEqualTo("deleted", false))
        .build();
    return super.selectByExample(ex);
  }
}