/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.service.impl;

import net.d2dcloud.entity.Role;
import net.d2dcloud.core.tk.service.impl.BaseServiceImpl;
import net.d2dcloud.service.RoleService;

import org.springframework.stereotype.Service;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.util.Sqls;

import java.util.List;

/**
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@Service
public class RoleServiceImpl extends BaseServiceImpl<Role> implements RoleService {


  @Override
  public Role selectById(Object id) {
    Role role = new Role();
    role.setId(id.toString());
    role.setDeleted(false);
    return selectOne(role);
  }

  @Override
  public Integer deleteById(String id) {
    Role role = new Role();
    role.setDeleted(true);
    Example ex = Example.builder(Role.class)
        .where(Sqls.custom()
            .andEqualTo("id", id))
        .build();
    return super.updatedByExampleSelective(role, ex);
  }

  @Override
  public List<Role> selectByIds(List<String> ids) {
    Example ex = Example.builder(Role.class)
        .where(Sqls.custom()
            .andIn("id", ids)
            .andEqualTo("deleted", false))
        .build();
    return super.selectByExample(ex);
  }
}