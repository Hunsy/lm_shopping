/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.service;

import net.d2dcloud.entity.System;
import net.d2dcloud.core.tk.service.BaseService;

import java.util.List;

/**
 *
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
public interface SystemService extends BaseService<System> {


  /**
   * 获取System列表
   *
   * @param ids
   * @return
   */
  List<System> selectByIds(List<String> ids);
}