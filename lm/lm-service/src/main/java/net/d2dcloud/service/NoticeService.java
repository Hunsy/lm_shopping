/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.service;

import net.d2dcloud.entity.Notice;
import net.d2dcloud.core.tk.service.BaseService;

import java.util.List;

/**
 *
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
public interface NoticeService extends BaseService<Notice> {


  /**
   * 获取Notice列表
   *
   * @param ids
   * @return
   */
  List<Notice> selectByIds(List<String> ids);
}