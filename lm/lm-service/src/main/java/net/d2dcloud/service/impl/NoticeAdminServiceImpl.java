/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.service.impl;

import net.d2dcloud.entity.NoticeAdmin;
import net.d2dcloud.core.tk.service.impl.BaseServiceImpl;
import net.d2dcloud.service.NoticeAdminService;

import org.springframework.stereotype.Service;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.util.Sqls;

import java.util.List;

/**
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@Service
public class NoticeAdminServiceImpl extends BaseServiceImpl<NoticeAdmin> implements NoticeAdminService {


  @Override
  public NoticeAdmin selectById(Object id) {
    NoticeAdmin noticeAdmin = new NoticeAdmin();
    noticeAdmin.setId(id.toString());
    noticeAdmin.setDeleted(false);
    return selectOne(noticeAdmin);
  }

  @Override
  public Integer deleteById(String id) {
    NoticeAdmin noticeAdmin = new NoticeAdmin();
    noticeAdmin.setDeleted(true);
    Example ex = Example.builder(NoticeAdmin.class)
        .where(Sqls.custom()
            .andEqualTo("id", id))
        .build();
    return super.updatedByExampleSelective(noticeAdmin, ex);
  }

  @Override
  public List<NoticeAdmin> selectByIds(List<String> ids) {
    Example ex = Example.builder(NoticeAdmin.class)
        .where(Sqls.custom()
            .andIn("id", ids)
            .andEqualTo("deleted", false))
        .build();
    return super.selectByExample(ex);
  }
}