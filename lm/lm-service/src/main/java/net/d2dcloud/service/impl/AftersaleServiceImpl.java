/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.service.impl;

import net.d2dcloud.entity.Aftersale;
import net.d2dcloud.core.tk.service.impl.BaseServiceImpl;
import net.d2dcloud.service.AftersaleService;

import org.springframework.stereotype.Service;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.util.Sqls;

import java.util.List;

/**
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@Service
public class AftersaleServiceImpl extends BaseServiceImpl<Aftersale> implements AftersaleService {


  @Override
  public Aftersale selectById(Object id) {
    Aftersale aftersale = new Aftersale();
    aftersale.setId(id.toString());
    aftersale.setDeleted(false);
    return selectOne(aftersale);
  }

  @Override
  public Integer deleteById(String id) {
    Aftersale aftersale = new Aftersale();
    aftersale.setDeleted(true);
    Example ex = Example.builder(Aftersale.class)
        .where(Sqls.custom()
            .andEqualTo("id", id))
        .build();
    return super.updatedByExampleSelective(aftersale, ex);
  }

  @Override
  public List<Aftersale> selectByIds(List<String> ids) {
    Example ex = Example.builder(Aftersale.class)
        .where(Sqls.custom()
            .andIn("id", ids)
            .andEqualTo("deleted", false))
        .build();
    return super.selectByExample(ex);
  }
}