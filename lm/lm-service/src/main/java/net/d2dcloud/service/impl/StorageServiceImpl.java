/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.service.impl;

import net.d2dcloud.entity.Storage;
import net.d2dcloud.core.tk.service.impl.BaseServiceImpl;
import net.d2dcloud.service.StorageService;

import org.springframework.stereotype.Service;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.util.Sqls;

import java.util.List;

/**
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@Service
public class StorageServiceImpl extends BaseServiceImpl<Storage> implements StorageService {


  @Override
  public Storage selectById(Object id) {
    Storage storage = new Storage();
    storage.setId(id.toString());
    storage.setDeleted(false);
    return selectOne(storage);
  }

  @Override
  public Integer deleteById(String id) {
    Storage storage = new Storage();
    storage.setDeleted(true);
    Example ex = Example.builder(Storage.class)
        .where(Sqls.custom()
            .andEqualTo("id", id))
        .build();
    return super.updatedByExampleSelective(storage, ex);
  }

  @Override
  public List<Storage> selectByIds(List<String> ids) {
    Example ex = Example.builder(Storage.class)
        .where(Sqls.custom()
            .andIn("id", ids)
            .andEqualTo("deleted", false))
        .build();
    return super.selectByExample(ex);
  }
}