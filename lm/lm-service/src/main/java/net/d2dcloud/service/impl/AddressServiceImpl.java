/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.service.impl;

import net.d2dcloud.core.tk.service.impl.BaseServiceImpl;
import net.d2dcloud.entity.Address;
import net.d2dcloud.service.AddressService;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.util.Sqls;

import java.util.List;

/**
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@Service
public class AddressServiceImpl extends BaseServiceImpl<Address> implements AddressService {


  @Override
  public Address selectById(Object id) {
    Address address = new Address ();
    address.setId (id.toString ());
    address.setDeleted (false);
    return selectOne (address);
  }

  @Override
  public Integer deleteById(String id) {
    Address address = new Address ();
    address.setDeleted (true);
    Example ex = Example.builder (Address.class)
      .where (Sqls.custom ()
        .andEqualTo ("id", id))
      .build ();
    return super.updatedByExampleSelective (address, ex);
  }

  @Override
  public List<Address> selectByIds(List<String> ids) {
    Example ex = Example.builder (Address.class)
      .where (Sqls.custom ()
        .andIn ("id", ids)
        .andEqualTo ("deleted", false))
      .build ();
    return super.selectByExample (ex);
  }
}