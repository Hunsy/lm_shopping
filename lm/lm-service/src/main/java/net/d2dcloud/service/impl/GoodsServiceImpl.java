/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.service.impl;

import net.d2dcloud.entity.Goods;
import net.d2dcloud.core.tk.service.impl.BaseServiceImpl;
import net.d2dcloud.service.GoodsService;

import org.springframework.stereotype.Service;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.util.Sqls;

import java.util.List;

/**
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@Service
public class GoodsServiceImpl extends BaseServiceImpl<Goods> implements GoodsService {


  @Override
  public Goods selectById(Object id) {
    Goods goods = new Goods();
    goods.setId(id.toString());
    goods.setDeleted(false);
    return selectOne(goods);
  }

  @Override
  public Integer deleteById(String id) {
    Goods goods = new Goods();
    goods.setDeleted(true);
    Example ex = Example.builder(Goods.class)
        .where(Sqls.custom()
            .andEqualTo("id", id))
        .build();
    return super.updatedByExampleSelective(goods, ex);
  }

  @Override
  public List<Goods> selectByIds(List<String> ids) {
    Example ex = Example.builder(Goods.class)
        .where(Sqls.custom()
            .andIn("id", ids)
            .andEqualTo("deleted", false))
        .build();
    return super.selectByExample(ex);
  }
}