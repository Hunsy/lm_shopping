/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import net.d2dcloud.core.tk.entity.BaseEntity;

import java.util.Date;

/**
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class Admin extends BaseEntity {

  /**
   * 管理员名称
   */
  private String username;
  /**
   * 管理员密码
   */
  private String password;
  /**
   * 最近一次登录IP地址
   */
  private String lastLoginIp;
  /**
   * 最近一次登录时间
   */
  private Date lastLoginTime;
  /**
   * 头像图片
   */
  private String avatar;
  /**
   * 创建时间
   */
  private Date addTime;
  /**
   * 更新时间
   */
  private Date updateTime;
  /**
   * 逻辑删除
   */
  private Boolean deleted;
  /**
   * 角色列表
   */
  private String roleIds;
}


