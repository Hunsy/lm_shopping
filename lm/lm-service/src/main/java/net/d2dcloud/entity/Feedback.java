/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import net.d2dcloud.core.tk.entity.BaseEntity;

import java.util.Date;

/**
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class Feedback extends BaseEntity {

  /**
   * 用户表的用户ID
   */
  private Integer userId;
  /**
   * 用户名称
   */
  private String username;
  /**
   * 手机号
   */
  private String mobile;
  /**
   * 反馈类型
   */
  private String feedType;
  /**
   * 反馈内容
   */
  private String content;
  /**
   * 状态
   */
  private Integer status;
  /**
   * 是否含有图片
   */
  private Boolean hasPicture;
  /**
   * 图片地址列表，采用JSON数组格式
   */
  private String picUrls;
  /**
   * 创建时间
   */
  private Date addTime;
  /**
   * 更新时间
   */
  private Date updateTime;
  /**
   * 逻辑删除
   */
  private Boolean deleted;
}


