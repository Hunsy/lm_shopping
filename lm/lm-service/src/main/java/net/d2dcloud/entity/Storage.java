/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.entity;

import net.d2dcloud.core.tk.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 *
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class Storage extends BaseEntity {

	/**
	 * 文件的唯一索引
	 */
	private String key;
	/**
	 * 文件名
	 */
	private String name;
	/**
	 * 文件类型
	 */
	private String type;
	/**
	 * 文件大小
	 */
	private Integer size;
	/**
	 * 文件访问链接
	 */
	private String url;
	/**
	 * 创建时间
	 */
	private Date addTime;
	/**
	 * 更新时间
	 */
	private Date updateTime;
	/**
	 * 逻辑删除
	 */
	private Boolean deleted;
}


