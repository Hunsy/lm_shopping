/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import net.d2dcloud.core.tk.entity.BaseEntity;

import java.util.Date;

/**
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class Address extends BaseEntity {

  /**
   * 收货人名称
   */
  private String name;
  /**
   * 用户表的用户ID
   */
  private Integer userId;
  /**
   * 行政区域表的省ID
   */
  private String province;
  /**
   * 行政区域表的市ID
   */
  private String city;
  /**
   * 行政区域表的区县ID
   */
  private String county;
  /**
   * 详细收货地址
   */
  private String addressDetail;
  /**
   * 地区编码
   */
  private String areaCode;
  /**
   * 邮政编码
   */
  private String postalCode;
  /**
   * 手机号码
   */
  private String tel;
  /**
   * 是否默认地址
   */
  private Boolean isDefault;
  /**
   * 创建时间
   */
  private Date addTime;
  /**
   * 更新时间
   */
  private Date updateTime;
  /**
   * 逻辑删除
   */
  private Boolean deleted;
}


