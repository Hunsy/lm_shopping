/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.entity;

import net.d2dcloud.core.tk.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 *
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class User extends BaseEntity {

	/**
	 * 用户名称
	 */
	private String username;
	/**
	 * 用户密码
	 */
	private String password;
	/**
	 * 性别：0 未知， 1男， 1 女
	 */
	private Integer gender;
	/**
	 * 生日
	 */
	private Date birthday;
	/**
	 * 最近一次登录时间
	 */
	private Date lastLoginTime;
	/**
	 * 最近一次登录IP地址
	 */
	private String lastLoginIp;
	/**
	 * 0 普通用户，1 VIP用户，2 高级VIP用户
	 */
	private Integer userLevel;
	/**
	 * 用户昵称或网络名称
	 */
	private String nickname;
	/**
	 * 用户手机号码
	 */
	private String mobile;
	/**
	 * 用户头像图片
	 */
	private String avatar;
	/**
	 * 微信登录openid
	 */
	private String weixinOpenid;
	/**
	 * 微信登录会话KEY
	 */
	private String sessionKey;
	/**
	 * 0 可用, 1 禁用, 2 注销
	 */
	private Integer status;
	/**
	 * 创建时间
	 */
	private Date addTime;
	/**
	 * 更新时间
	 */
	private Date updateTime;
	/**
	 * 逻辑删除
	 */
	private Boolean deleted;
}


