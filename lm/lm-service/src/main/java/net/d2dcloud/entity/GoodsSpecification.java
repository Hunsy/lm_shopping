/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import net.d2dcloud.core.tk.entity.BaseEntity;

import java.util.Date;

/**
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class GoodsSpecification extends BaseEntity {

  /**
   * 商品表的商品ID
   */
  private Integer goodsId;
  /**
   * 商品规格名称
   */
  private String specification;
  /**
   * 商品规格值
   */
  private String value;
  /**
   * 商品规格图片
   */
  private String picUrl;
  /**
   * 创建时间
   */
  private Date addTime;
  /**
   * 更新时间
   */
  private Date updateTime;
  /**
   * 逻辑删除
   */
  private Boolean deleted;
}


