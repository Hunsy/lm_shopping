/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import net.d2dcloud.core.tk.entity.BaseEntity;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class Brand extends BaseEntity {

  /**
   * 品牌商名称
   */
  private String name;
  /**
   * 品牌商简介
   */
  private String desc;
  /**
   * 品牌商页的品牌商图片
   */
  private String picUrl;
  /**
   *
   */
  private Integer sortOrder;
  /**
   * 品牌商的商品低价，仅用于页面展示
   */
  private BigDecimal floorPrice;
  /**
   * 创建时间
   */
  private Date addTime;
  /**
   * 更新时间
   */
  private Date updateTime;
  /**
   * 逻辑删除
   */
  private Boolean deleted;
}


