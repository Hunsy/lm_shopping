/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import net.d2dcloud.core.tk.entity.BaseEntity;

import java.util.Date;

/**
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class Log extends BaseEntity {

  /**
   * 管理员
   */
  private String admin;
  /**
   * 管理员地址
   */
  private String ip;
  /**
   * 操作分类
   */
  private Integer type;
  /**
   * 操作动作
   */
  private String action;
  /**
   * 操作状态
   */
  private Boolean status;
  /**
   * 操作结果，或者成功消息，或者失败消息
   */
  private String result;
  /**
   * 补充信息
   */
  private String comment;
  /**
   * 创建时间
   */
  private Date addTime;
  /**
   * 更新时间
   */
  private Date updateTime;
  /**
   * 逻辑删除
   */
  private Boolean deleted;
}


