/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import net.d2dcloud.core.tk.entity.BaseEntity;

import java.util.Date;

/**
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class Keyword extends BaseEntity {

  /**
   * 关键字
   */
  private String keyword;
  /**
   * 关键字的跳转链接
   */
  private String url;
  /**
   * 是否是热门关键字
   */
  private Boolean isHot;
  /**
   * 是否是默认关键字
   */
  private Boolean isDefault;
  /**
   * 排序
   */
  private Integer sortOrder;
  /**
   * 创建时间
   */
  private Date addTime;
  /**
   * 更新时间
   */
  private Date updateTime;
  /**
   * 逻辑删除
   */
  private Boolean deleted;
}


