/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import net.d2dcloud.core.tk.entity.BaseEntity;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class Goods extends BaseEntity {

  /**
   * 商品编号
   */
  private String goodsSn;
  /**
   * 商品名称
   */
  private String name;
  /**
   * 商品所属类目ID
   */
  private Integer categoryId;
  /**
   *
   */
  private Integer brandId;
  /**
   * 商品宣传图片列表，采用JSON数组格式
   */
  private String gallery;
  /**
   * 商品关键字，采用逗号间隔
   */
  private String keywords;
  /**
   * 商品简介
   */
  private String brief;
  /**
   * 是否上架
   */
  private Boolean isOnSale;
  /**
   *
   */
  private Integer sortOrder;
  /**
   * 商品页面商品图片
   */
  private String picUrl;
  /**
   * 商品分享朋友圈图片
   */
  private String shareUrl;
  /**
   * 是否新品首发，如果设置则可以在新品首发页面展示
   */
  private Boolean isNew;
  /**
   * 是否人气推荐，如果设置则可以在人气推荐页面展示
   */
  private Boolean isHot;
  /**
   * 商品单位，例如件、盒
   */
  private String unit;
  /**
   * 专柜价格
   */
  private BigDecimal counterPrice;
  /**
   * 零售价格
   */
  private BigDecimal retailPrice;
  /**
   * 商品详细介绍，是富文本格式
   */
  private String detail;
  /**
   * 创建时间
   */
  private Date addTime;
  /**
   * 更新时间
   */
  private Date updateTime;
  /**
   * 逻辑删除
   */
  private Boolean deleted;
}


