/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.entity;

import net.d2dcloud.core.tk.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class Topic extends BaseEntity {

	/**
	 * 专题标题
	 */
	private String title;
	/**
	 * 专题子标题
	 */
	private String subtitle;
	/**
	 * 专题内容，富文本格式
	 */
	private String content;
	/**
	 * 专题相关商品最低价
	 */
	private BigDecimal price;
	/**
	 * 专题阅读量
	 */
	private String readCount;
	/**
	 * 专题图片
	 */
	private String picUrl;
	/**
	 * 排序
	 */
	private Integer sortOrder;
	/**
	 * 专题相关商品，采用JSON数组格式
	 */
	private String goods;
	/**
	 * 创建时间
	 */
	private Date addTime;
	/**
	 * 更新时间
	 */
	private Date updateTime;
	/**
	 * 逻辑删除
	 */
	private Boolean deleted;
}


