/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import net.d2dcloud.core.tk.entity.BaseEntity;

import java.util.Date;

/**
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class NoticeAdmin extends BaseEntity {

  /**
   * 通知ID
   */
  private Integer noticeId;
  /**
   * 通知标题
   */
  private String noticeTitle;
  /**
   * 接收通知的管理员ID
   */
  private Integer adminId;
  /**
   * 阅读时间，如果是NULL则是未读状态
   */
  private Date readTime;
  /**
   * 创建时间
   */
  private Date addTime;
  /**
   * 更新时间
   */
  private Date updateTime;
  /**
   * 逻辑删除
   */
  private Boolean deleted;
}


