/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import net.d2dcloud.core.tk.entity.BaseEntity;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class GoodsProduct extends BaseEntity {

  /**
   * 商品表的商品ID
   */
  private Integer goodsId;
  /**
   * 商品规格值列表，采用JSON数组格式
   */
  private String specifications;
  /**
   * 商品货品价格
   */
  private BigDecimal price;
  /**
   * 商品货品数量
   */
  private Integer number;
  /**
   * 商品货品图片
   */
  private String url;
  /**
   * 创建时间
   */
  private Date addTime;
  /**
   * 更新时间
   */
  private Date updateTime;
  /**
   * 逻辑删除
   */
  private Boolean deleted;
}


