/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import net.d2dcloud.core.tk.entity.BaseEntity;

import java.util.Date;

/**
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class Collect extends BaseEntity {

  /**
   * 用户表的用户ID
   */
  private Integer userId;
  /**
   * 如果type=0，则是商品ID；如果type=1，则是专题ID
   */
  private Integer valueId;
  /**
   * 收藏类型，如果type=0，则是商品ID；如果type=1，则是专题ID
   */
  private Integer type;
  /**
   * 创建时间
   */
  private Date addTime;
  /**
   * 更新时间
   */
  private Date updateTime;
  /**
   * 逻辑删除
   */
  private Boolean deleted;
}


