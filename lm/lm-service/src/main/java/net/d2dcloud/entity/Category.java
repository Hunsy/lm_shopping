/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import net.d2dcloud.core.tk.entity.BaseEntity;

import java.util.Date;

/**
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class Category extends BaseEntity {

  /**
   * 类目名称
   */
  private String name;
  /**
   * 类目关键字，以JSON数组格式
   */
  private String keywords;
  /**
   * 类目广告语介绍
   */
  private String desc;
  /**
   * 父类目ID
   */
  private Integer pid;
  /**
   * 类目图标
   */
  private String iconUrl;
  /**
   * 类目图片
   */
  private String picUrl;
  /**
   *
   */
  private String level;
  /**
   * 排序
   */
  private Integer sortOrder;
  /**
   * 创建时间
   */
  private Date addTime;
  /**
   * 更新时间
   */
  private Date updateTime;
  /**
   * 逻辑删除
   */
  private Boolean deleted;
}


