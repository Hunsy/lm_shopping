/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import net.d2dcloud.core.tk.entity.BaseEntity;

import java.util.Date;

/**
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class Comment extends BaseEntity {

  /**
   * 如果type=0，则是商品评论；如果是type=1，则是专题评论。
   */
  private Integer valueId;
  /**
   * 评论类型，如果type=0，则是商品评论；如果是type=1，则是专题评论；
   */
  private Integer type;
  /**
   * 评论内容
   */
  private String content;
  /**
   * 管理员回复内容
   */
  private String adminContent;
  /**
   * 用户表的用户ID
   */
  private Integer userId;
  /**
   * 是否含有图片
   */
  private Boolean hasPicture;
  /**
   * 图片地址列表，采用JSON数组格式
   */
  private String picUrls;
  /**
   * 评分， 1-5
   */
  private Integer star;
  /**
   * 创建时间
   */
  private Date addTime;
  /**
   * 更新时间
   */
  private Date updateTime;
  /**
   * 逻辑删除
   */
  private Boolean deleted;
}


