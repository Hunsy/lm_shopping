/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.api.vo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.io.Serializable;

/**
 *
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@Data
public class TopicVO implements Serializable {

	//columns START
	/**
	 * 专题标题
	 */
	@Size(max = 255)
	private String title;
	/**
	 * 专题子标题
	 */
	@NotNull
	@Size(max = 255)
	private String subtitle;
	/**
	 * 专题内容，富文本格式
	 */
	@NotNull
	@Size(max = 65535)
	private String content;
	/**
	 * 专题相关商品最低价
	 */
	@NotNull
	private BigDecimal price;
	/**
	 * 专题阅读量
	 */
	@NotNull
	@Size(max = 255)
	private String readCount;
	/**
	 * 专题图片
	 */
	@NotNull
	@Size(max = 255)
	private String picUrl;
	/**
	 * 排序
	 */
	@NotNull
	private Integer sortOrder;
	/**
	 * 专题相关商品，采用JSON数组格式
	 */
	@NotNull
	@Size(max = 1023)
	private String goods;
	/**
	 * 创建时间
	 */
	@NotNull
	private Date addTime;
	/**
	 * 更新时间
	 */
	@NotNull
	private Date updateTime;
	/**
	 * 逻辑删除
	 */
	@NotNull
	private Boolean deleted;
	//columns END
}


