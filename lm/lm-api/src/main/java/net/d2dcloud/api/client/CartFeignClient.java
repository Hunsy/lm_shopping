/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.api.client;

import net.d2dcloud.api.resp.LmPgRet;
import net.d2dcloud.api.resp.LmResult;

import net.d2dcloud.api.vo.CartVO;
import net.d2dcloud.api.vo.CartDetailVO;
import net.d2dcloud.api.vo.CartUpdateVO;
import net.d2dcloud.api.vo.CartItemVO;
import net.d2dcloud.api.vo.CartSearchVO;

import org.springframework.web.bind.annotation.*;
import org.springframework.cloud.openfeign.FeignClient;

import javax.validation.Valid;

import java.util.List;

/**
 *
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@FeignClient("lm-service")
public interface CartFeignClient  {

  /**
   * 新增Cart
   *
   * @param cartVO
   * @return
   */
  @PostMapping("/cart")
  LmResult<Boolean> add(@Valid @RequestBody CartVO cartVO);

  /**
   * 修改Cart
   *
   * @param cartUpdateVO
   * @return
   */
  @PutMapping("/cart")
  Resp<Boolean> update(@Valid @RequestBody CartUpdateVO cartUpdateVO) ;

  /**
   * 删除Cart
   *
   * @param id
   * @return
   */
  @DeleteMapping("/cart/{id}")
  Resp<Boolean> delete(@PathVariable("id")String id) ;

  /**
   * 获取Cart详情
   *
   * @param id
   * @return
   */
  @GetMapping("/cart/{id}")
  Resp<CartDetailVO> get(@PathVariable("id")String id) ;

  /**
   * 获取Cart详情列表
   *
   * @param ids
   * @return
   */
  @GetMapping("/cart/list")
  Resp<List<CartItemVO>> getList(List<String> ids) ;

  /**
   * 获取Cart分页列表
   *
   * @param cartSearchVO
   * @return
   */
  @GetMapping("/cart/page")
  Resp<PgRet<CartItemVO>> getPage(CartSearchVO cartSearchVO);

}