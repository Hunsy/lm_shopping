/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.api.vo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.io.Serializable;

/**
 *
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@Data
public class OrderVO implements Serializable {

	//columns START
	/**
	 * 用户表的用户ID
	 */
	private Integer userId;
	/**
	 * 订单编号
	 */
	@Size(max = 63)
	private String orderSn;
	/**
	 * 订单状态
	 */
	private Integer orderStatus;
	/**
	 * 售后状态，0是可申请，1是用户已申请，2是管理员审核通过，3是管理员退款成功，4是管理员审核拒绝，5是用户已取消
	 */
	@NotNull
	private Integer aftersaleStatus;
	/**
	 * 收货人名称
	 */
	@Size(max = 63)
	private String consignee;
	/**
	 * 收货人手机号
	 */
	@Size(max = 63)
	private String mobile;
	/**
	 * 收货具体地址
	 */
	@Size(max = 127)
	private String address;
	/**
	 * 用户订单留言
	 */
	@Size(max = 512)
	private String message;
	/**
	 * 商品总费用
	 */
	private BigDecimal goodsPrice;
	/**
	 * 配送费用
	 */
	private BigDecimal freightPrice;
	/**
	 * 优惠券减免
	 */
	private BigDecimal couponPrice;
	/**
	 * 用户积分减免
	 */
	private BigDecimal integralPrice;
	/**
	 * 团购优惠价减免
	 */
	private BigDecimal grouponPrice;
	/**
	 * 订单费用， = goods_price + freight_price - coupon_price
	 */
	private BigDecimal orderPrice;
	/**
	 * 实付费用， = order_price - integral_price
	 */
	private BigDecimal actualPrice;
	/**
	 * 微信付款编号
	 */
	@NotNull
	@Size(max = 63)
	private String payId;
	/**
	 * 微信付款时间
	 */
	@NotNull
	private Date payTime;
	/**
	 * 发货编号
	 */
	@NotNull
	@Size(max = 63)
	private String shipSn;
	/**
	 * 发货快递公司
	 */
	@NotNull
	@Size(max = 63)
	private String shipChannel;
	/**
	 * 发货开始时间
	 */
	@NotNull
	private Date shipTime;
	/**
	 * 实际退款金额，（有可能退款金额小于实际支付金额）
	 */
	@NotNull
	private BigDecimal refundAmount;
	/**
	 * 退款方式
	 */
	@NotNull
	@Size(max = 63)
	private String refundType;
	/**
	 * 退款备注
	 */
	@NotNull
	@Size(max = 127)
	private String refundContent;
	/**
	 * 退款时间
	 */
	@NotNull
	private Date refundTime;
	/**
	 * 用户确认收货时间
	 */
	@NotNull
	private Date confirmTime;
	/**
	 * 待评价订单商品数量
	 */
	@NotNull
	private Integer comments;
	/**
	 * 订单关闭时间
	 */
	@NotNull
	private Date endTime;
	/**
	 * 创建时间
	 */
	@NotNull
	private Date addTime;
	/**
	 * 更新时间
	 */
	@NotNull
	private Date updateTime;
	/**
	 * 逻辑删除
	 */
	@NotNull
	private Boolean deleted;
	//columns END
}


