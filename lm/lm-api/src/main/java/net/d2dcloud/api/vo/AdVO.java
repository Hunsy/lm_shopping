/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.api.vo;

import lombok.Data;

import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.io.Serializable;

/**
 *
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@Data
public class AdVO implements Serializable {

	//columns START
	/**
	 * 广告标题
	 */
	@Size(max = 63)
	private String name;
	/**
	 * 所广告的商品页面或者活动页面链接地址
	 */
	@Size(max = 255)
	private String link;
	/**
	 * 广告宣传图片
	 */
	@Size(max = 255)
	private String url;
	/**
	 * 广告位置：1则是首页
	 */
	@NotNull
	private Integer position;
	/**
	 * 活动内容
	 */
	@NotNull
	@Size(max = 255)
	private String content;
	/**
	 * 广告开始时间
	 */
	@NotNull
	private Date startTime;
	/**
	 * 广告结束时间
	 */
	@NotNull
	private Date endTime;
	/**
	 * 是否启动
	 */
	@NotNull
	private Boolean enabled;
	/**
	 * 创建时间
	 */
	@NotNull
	private Date addTime;
	/**
	 * 更新时间
	 */
	@NotNull
	private Date updateTime;
	/**
	 * 逻辑删除
	 */
	@NotNull
	private Boolean deleted;
	//columns END
}


