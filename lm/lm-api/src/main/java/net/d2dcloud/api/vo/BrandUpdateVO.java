/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.api.vo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.io.Serializable;

/**
 *  
 *
 * @author codeGen_1.0.0
 */
@Data
public class BrandUpdateVO implements Serializable {

	//columns START
	@NotNull
	private String id;
	/**
	 * 品牌商名称
	 */
	@Size(max = 255)
	private String name;
	/**
	 * 品牌商简介
	 */
	@Size(max = 255)
	private String desc;
	/**
	 * 品牌商页的品牌商图片
	 */
	@Size(max = 255)
	private String picUrl;
	/**
	 * 
	 */
	private Integer sortOrder;
	/**
	 * 品牌商的商品低价，仅用于页面展示
	 */
	private BigDecimal floorPrice;
	/**
	 * 创建时间
	 */
	private Date addTime;
	/**
	 * 更新时间
	 */
	private Date updateTime;
	/**
	 * 逻辑删除
	 */
	private Boolean deleted;
	//columns END
}


