/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.api.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

import net.d2dcloud.resp.CouponUserPgInVo;

/**
 *  
 *
 * @author codeGen_1.0.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class CouponUserSearchVO extends CouponUserPgInVo {

	//columns START
	/**
	 * 
	 */
	private String id;
	/**
	 * 用户ID
	 */
	private Integer userId;
	/**
	 * 优惠券ID
	 */
	private Integer couponId;
	/**
	 * 使用状态, 如果是0则未使用；如果是1则已使用；如果是2则已过期；如果是3则已经下架；
	 */
	private Integer status;
	/**
	 * 使用时间
	 */
	private Date usedTime;
	/**
	 * 有效期开始时间
	 */
	private Date startTime;
	/**
	 * 有效期截至时间
	 */
	private Date endTime;
	/**
	 * 订单ID
	 */
	private Integer orderId;
	/**
	 * 创建时间
	 */
	private Date addTime;
	/**
	 * 更新时间
	 */
	private Date updateTime;
	/**
	 * 逻辑删除
	 */
	private Boolean deleted;
	//columns END
}


