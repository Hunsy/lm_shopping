/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.api.vo;

import lombok.Data;

import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.io.Serializable;

/**
 *
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@Data
public class CommentVO implements Serializable {

	//columns START
	/**
	 * 如果type=0，则是商品评论；如果是type=1，则是专题评论。
	 */
	private Integer valueId;
	/**
	 * 评论类型，如果type=0，则是商品评论；如果是type=1，则是专题评论；
	 */
	private Integer type;
	/**
	 * 评论内容
	 */
	@Size(max = 1023)
	private String content;
	/**
	 * 管理员回复内容
	 */
	@Size(max = 511)
	private String adminContent;
	/**
	 * 用户表的用户ID
	 */
	private Integer userId;
	/**
	 * 是否含有图片
	 */
	@NotNull
	private Boolean hasPicture;
	/**
	 * 图片地址列表，采用JSON数组格式
	 */
	@NotNull
	@Size(max = 1023)
	private String picUrls;
	/**
	 * 评分， 1-5
	 */
	@NotNull
	private Integer star;
	/**
	 * 创建时间
	 */
	@NotNull
	private Date addTime;
	/**
	 * 更新时间
	 */
	@NotNull
	private Date updateTime;
	/**
	 * 逻辑删除
	 */
	@NotNull
	private Boolean deleted;
	//columns END
}


