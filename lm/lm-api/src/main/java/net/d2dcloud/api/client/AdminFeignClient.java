/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.api.client;

import net.d2dcloud.api.vo.*;
import net.d2dcloud.core.resp.PgRet;
import net.d2dcloud.core.resp.Resp;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@FeignClient("lm-service")
public interface AdminFeignClient {

  /**
   * 新增Admin
   *
   * @param adminVO
   * @return
   */
  @PostMapping("/admin")
  Resp<Boolean> add(@Valid @RequestBody AdminVO adminVO);

  /**
   * 修改Admin
   *
   * @param adminUpdateVO
   * @return
   */
  @PutMapping("/admin")
  Resp<Boolean> update(@Valid @RequestBody AdminUpdateVO adminUpdateVO);

  /**
   * 删除Admin
   *
   * @param id
   * @return
   */
  @DeleteMapping("/admin/{id}")
  Resp<Boolean> delete(@PathVariable("id") String id);

  /**
   * 获取Admin详情
   *
   * @param id
   * @return
   */
  @GetMapping("/admin/{id}")
  Resp<AdminDetailVO> get(@PathVariable("id") String id);

  /**
   * 获取Admin详情列表
   *
   * @param ids
   * @return
   */
  @GetMapping("/admin/list")
  Resp<List<AdminItemVO>> getList(List<String> ids);

  /**
   * 获取Admin分页列表
   *
   * @param adminSearchVO
   * @return
   */
  @GetMapping("/admin/page")
  Resp<PgRet<AdminItemVO>> getPage(AdminSearchVO adminSearchVO);

}