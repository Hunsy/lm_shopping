/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.api.vo;

import lombok.Data;

import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.io.Serializable;

/**
 *
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@Data
public class LogVO implements Serializable {

	//columns START
	/**
	 * 管理员
	 */
	@NotNull
	@Size(max = 45)
	private String admin;
	/**
	 * 管理员地址
	 */
	@NotNull
	@Size(max = 45)
	private String ip;
	/**
	 * 操作分类
	 */
	@NotNull
	private Integer type;
	/**
	 * 操作动作
	 */
	@NotNull
	@Size(max = 45)
	private String action;
	/**
	 * 操作状态
	 */
	@NotNull
	private Boolean status;
	/**
	 * 操作结果，或者成功消息，或者失败消息
	 */
	@NotNull
	@Size(max = 127)
	private String result;
	/**
	 * 补充信息
	 */
	@NotNull
	@Size(max = 255)
	private String comment;
	/**
	 * 创建时间
	 */
	@NotNull
	private Date addTime;
	/**
	 * 更新时间
	 */
	@NotNull
	private Date updateTime;
	/**
	 * 逻辑删除
	 */
	@NotNull
	private Boolean deleted;
	//columns END
}


