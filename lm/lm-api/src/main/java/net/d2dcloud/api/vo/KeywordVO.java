/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.api.vo;

import lombok.Data;

import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.io.Serializable;

/**
 *
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@Data
public class KeywordVO implements Serializable {

	//columns START
	/**
	 * 关键字
	 */
	@Size(max = 127)
	private String keyword;
	/**
	 * 关键字的跳转链接
	 */
	@Size(max = 255)
	private String url;
	/**
	 * 是否是热门关键字
	 */
	private Boolean isHot;
	/**
	 * 是否是默认关键字
	 */
	private Boolean isDefault;
	/**
	 * 排序
	 */
	private Integer sortOrder;
	/**
	 * 创建时间
	 */
	@NotNull
	private Date addTime;
	/**
	 * 更新时间
	 */
	@NotNull
	private Date updateTime;
	/**
	 * 逻辑删除
	 */
	@NotNull
	private Boolean deleted;
	//columns END
}


