/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.api.client;

import net.d2dcloud.api.resp.LmPgRet;
import net.d2dcloud.api.resp.LmResult;

import net.d2dcloud.api.vo.FeedbackVO;
import net.d2dcloud.api.vo.FeedbackDetailVO;
import net.d2dcloud.api.vo.FeedbackUpdateVO;
import net.d2dcloud.api.vo.FeedbackItemVO;
import net.d2dcloud.api.vo.FeedbackSearchVO;

import org.springframework.web.bind.annotation.*;
import org.springframework.cloud.openfeign.FeignClient;

import javax.validation.Valid;

import java.util.List;

/**
 *
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@FeignClient("lm-service")
public interface FeedbackFeignClient  {

  /**
   * 新增Feedback
   *
   * @param feedbackVO
   * @return
   */
  @PostMapping("/feedback")
  LmResult<Boolean> add(@Valid @RequestBody FeedbackVO feedbackVO);

  /**
   * 修改Feedback
   *
   * @param feedbackUpdateVO
   * @return
   */
  @PutMapping("/feedback")
  Resp<Boolean> update(@Valid @RequestBody FeedbackUpdateVO feedbackUpdateVO) ;

  /**
   * 删除Feedback
   *
   * @param id
   * @return
   */
  @DeleteMapping("/feedback/{id}")
  Resp<Boolean> delete(@PathVariable("id")String id) ;

  /**
   * 获取Feedback详情
   *
   * @param id
   * @return
   */
  @GetMapping("/feedback/{id}")
  Resp<FeedbackDetailVO> get(@PathVariable("id")String id) ;

  /**
   * 获取Feedback详情列表
   *
   * @param ids
   * @return
   */
  @GetMapping("/feedback/list")
  Resp<List<FeedbackItemVO>> getList(List<String> ids) ;

  /**
   * 获取Feedback分页列表
   *
   * @param feedbackSearchVO
   * @return
   */
  @GetMapping("/feedback/page")
  Resp<PgRet<FeedbackItemVO>> getPage(FeedbackSearchVO feedbackSearchVO);

}