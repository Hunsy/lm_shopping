/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.api.vo;

import lombok.Data;

import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.io.Serializable;

/**
 *  
 *
 * @author codeGen_1.0.0
 */
@Data
public class LogUpdateVO implements Serializable {

	//columns START
	@NotNull
	private String id;
	/**
	 * 管理员
	 */
	@Size(max = 45)
	private String admin;
	/**
	 * 管理员地址
	 */
	@Size(max = 45)
	private String ip;
	/**
	 * 操作分类
	 */
	private Integer type;
	/**
	 * 操作动作
	 */
	@Size(max = 45)
	private String action;
	/**
	 * 操作状态
	 */
	private Boolean status;
	/**
	 * 操作结果，或者成功消息，或者失败消息
	 */
	@Size(max = 127)
	private String result;
	/**
	 * 补充信息
	 */
	@Size(max = 255)
	private String comment;
	/**
	 * 创建时间
	 */
	private Date addTime;
	/**
	 * 更新时间
	 */
	private Date updateTime;
	/**
	 * 逻辑删除
	 */
	private Boolean deleted;
	//columns END
}


