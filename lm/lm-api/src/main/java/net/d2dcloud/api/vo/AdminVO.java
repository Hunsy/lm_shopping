/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.api.vo;

import lombok.Data;

import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.io.Serializable;

/**
 *
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@Data
public class AdminVO implements Serializable {

	//columns START
	/**
	 * 管理员名称
	 */
	@Size(max = 63)
	private String username;
	/**
	 * 管理员密码
	 */
	@Size(max = 63)
	private String password;
	/**
	 * 最近一次登录IP地址
	 */
	@NotNull
	@Size(max = 63)
	private String lastLoginIp;
	/**
	 * 最近一次登录时间
	 */
	@NotNull
	private Date lastLoginTime;
	/**
	 * 头像图片
	 */
	@NotNull
	@Size(max = 255)
	private String avatar;
	/**
	 * 创建时间
	 */
	@NotNull
	private Date addTime;
	/**
	 * 更新时间
	 */
	@NotNull
	private Date updateTime;
	/**
	 * 逻辑删除
	 */
	@NotNull
	private Boolean deleted;
	/**
	 * 角色列表
	 */
	@NotNull
	@Size(max = 127)
	private String roleIds;
	//columns END
}


