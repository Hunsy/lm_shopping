/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.api.client;

import net.d2dcloud.api.resp.LmPgRet;
import net.d2dcloud.api.resp.LmResult;

import net.d2dcloud.api.vo.TopicVO;
import net.d2dcloud.api.vo.TopicDetailVO;
import net.d2dcloud.api.vo.TopicUpdateVO;
import net.d2dcloud.api.vo.TopicItemVO;
import net.d2dcloud.api.vo.TopicSearchVO;

import org.springframework.web.bind.annotation.*;
import org.springframework.cloud.openfeign.FeignClient;

import javax.validation.Valid;

import java.util.List;

/**
 *
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@FeignClient("lm-service")
public interface TopicFeignClient  {

  /**
   * 新增Topic
   *
   * @param topicVO
   * @return
   */
  @PostMapping("/topic")
  LmResult<Boolean> add(@Valid @RequestBody TopicVO topicVO);

  /**
   * 修改Topic
   *
   * @param topicUpdateVO
   * @return
   */
  @PutMapping("/topic")
  Resp<Boolean> update(@Valid @RequestBody TopicUpdateVO topicUpdateVO) ;

  /**
   * 删除Topic
   *
   * @param id
   * @return
   */
  @DeleteMapping("/topic/{id}")
  Resp<Boolean> delete(@PathVariable("id")String id) ;

  /**
   * 获取Topic详情
   *
   * @param id
   * @return
   */
  @GetMapping("/topic/{id}")
  Resp<TopicDetailVO> get(@PathVariable("id")String id) ;

  /**
   * 获取Topic详情列表
   *
   * @param ids
   * @return
   */
  @GetMapping("/topic/list")
  Resp<List<TopicItemVO>> getList(List<String> ids) ;

  /**
   * 获取Topic分页列表
   *
   * @param topicSearchVO
   * @return
   */
  @GetMapping("/topic/page")
  Resp<PgRet<TopicItemVO>> getPage(TopicSearchVO topicSearchVO);

}