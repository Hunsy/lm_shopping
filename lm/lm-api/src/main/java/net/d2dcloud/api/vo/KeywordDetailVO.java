/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.api.vo;

import lombok.Data;

import java.util.Date;

import java.io.Serializable;


/**
 *  
 *
 * @author codeGen_1.0.0
 */
@Data
public class KeywordDetailVO implements Serializable {

	//columns START
	/**
	 * 
	 */
	private String id;
	/**
	 * 关键字
	 */
	private String keyword;
	/**
	 * 关键字的跳转链接
	 */
	private String url;
	/**
	 * 是否是热门关键字
	 */
	private Boolean isHot;
	/**
	 * 是否是默认关键字
	 */
	private Boolean isDefault;
	/**
	 * 排序
	 */
	private Integer sortOrder;
	/**
	 * 创建时间
	 */
	private Date addTime;
	/**
	 * 更新时间
	 */
	private Date updateTime;
	/**
	 * 逻辑删除
	 */
	private Boolean deleted;
	//columns END
}


