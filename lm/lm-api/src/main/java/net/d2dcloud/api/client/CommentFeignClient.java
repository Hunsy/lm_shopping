/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.api.client;

import net.d2dcloud.api.resp.LmPgRet;
import net.d2dcloud.api.resp.LmResult;

import net.d2dcloud.api.vo.CommentVO;
import net.d2dcloud.api.vo.CommentDetailVO;
import net.d2dcloud.api.vo.CommentUpdateVO;
import net.d2dcloud.api.vo.CommentItemVO;
import net.d2dcloud.api.vo.CommentSearchVO;

import org.springframework.web.bind.annotation.*;
import org.springframework.cloud.openfeign.FeignClient;

import javax.validation.Valid;

import java.util.List;

/**
 *
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@FeignClient("lm-service")
public interface CommentFeignClient  {

  /**
   * 新增Comment
   *
   * @param commentVO
   * @return
   */
  @PostMapping("/comment")
  LmResult<Boolean> add(@Valid @RequestBody CommentVO commentVO);

  /**
   * 修改Comment
   *
   * @param commentUpdateVO
   * @return
   */
  @PutMapping("/comment")
  Resp<Boolean> update(@Valid @RequestBody CommentUpdateVO commentUpdateVO) ;

  /**
   * 删除Comment
   *
   * @param id
   * @return
   */
  @DeleteMapping("/comment/{id}")
  Resp<Boolean> delete(@PathVariable("id")String id) ;

  /**
   * 获取Comment详情
   *
   * @param id
   * @return
   */
  @GetMapping("/comment/{id}")
  Resp<CommentDetailVO> get(@PathVariable("id")String id) ;

  /**
   * 获取Comment详情列表
   *
   * @param ids
   * @return
   */
  @GetMapping("/comment/list")
  Resp<List<CommentItemVO>> getList(List<String> ids) ;

  /**
   * 获取Comment分页列表
   *
   * @param commentSearchVO
   * @return
   */
  @GetMapping("/comment/page")
  Resp<PgRet<CommentItemVO>> getPage(CommentSearchVO commentSearchVO);

}