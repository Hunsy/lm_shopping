/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.api.vo;

import lombok.Data;

import java.util.Date;

import java.io.Serializable;

/**
 *  
 *
 * @author codeGen_1.0.0
 */
@Data
public class GrouponItemVO implements Serializable {

	//columns START
	/**
	 * 
	 */
	private String id;
	/**
	 * 关联的订单ID
	 */
	private Integer orderId;
	/**
	 * 如果是开团用户，则groupon_id是0；如果是参团用户，则groupon_id是团购活动ID
	 */
	private Integer grouponId;
	/**
	 * 团购规则ID，关联groupon_rules表ID字段
	 */
	private Integer rulesId;
	/**
	 * 用户ID
	 */
	private Integer userId;
	/**
	 * 团购分享图片地址
	 */
	private String shareUrl;
	/**
	 * 开团用户ID
	 */
	private Integer creatorUserId;
	/**
	 * 开团时间
	 */
	private Date creatorUserTime;
	/**
	 * 团购活动状态，开团未支付则0，开团中则1，开团失败则2
	 */
	private Integer status;
	/**
	 * 创建时间
	 */
	private Date addTime;
	/**
	 * 更新时间
	 */
	private Date updateTime;
	/**
	 * 逻辑删除
	 */
	private Boolean deleted;
	//columns END
}


