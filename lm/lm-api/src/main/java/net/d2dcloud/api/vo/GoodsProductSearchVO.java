/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.api.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.util.Date;

import net.d2dcloud.resp.GoodsProductPgInVo;

/**
 *  
 *
 * @author codeGen_1.0.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class GoodsProductSearchVO extends GoodsProductPgInVo {

	//columns START
	/**
	 * 
	 */
	private String id;
	/**
	 * 商品表的商品ID
	 */
	private Integer goodsId;
	/**
	 * 商品规格值列表，采用JSON数组格式
	 */
	private String specifications;
	/**
	 * 商品货品价格
	 */
	private BigDecimal price;
	/**
	 * 商品货品数量
	 */
	private Integer number;
	/**
	 * 商品货品图片
	 */
	private String url;
	/**
	 * 创建时间
	 */
	private Date addTime;
	/**
	 * 更新时间
	 */
	private Date updateTime;
	/**
	 * 逻辑删除
	 */
	private Boolean deleted;
	//columns END
}


