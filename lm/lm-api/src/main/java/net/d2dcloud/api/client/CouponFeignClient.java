/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.api.client;

import net.d2dcloud.api.resp.LmPgRet;
import net.d2dcloud.api.resp.LmResult;

import net.d2dcloud.api.vo.CouponVO;
import net.d2dcloud.api.vo.CouponDetailVO;
import net.d2dcloud.api.vo.CouponUpdateVO;
import net.d2dcloud.api.vo.CouponItemVO;
import net.d2dcloud.api.vo.CouponSearchVO;

import org.springframework.web.bind.annotation.*;
import org.springframework.cloud.openfeign.FeignClient;

import javax.validation.Valid;

import java.util.List;

/**
 *
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@FeignClient("lm-service")
public interface CouponFeignClient  {

  /**
   * 新增Coupon
   *
   * @param couponVO
   * @return
   */
  @PostMapping("/coupon")
  LmResult<Boolean> add(@Valid @RequestBody CouponVO couponVO);

  /**
   * 修改Coupon
   *
   * @param couponUpdateVO
   * @return
   */
  @PutMapping("/coupon")
  Resp<Boolean> update(@Valid @RequestBody CouponUpdateVO couponUpdateVO) ;

  /**
   * 删除Coupon
   *
   * @param id
   * @return
   */
  @DeleteMapping("/coupon/{id}")
  Resp<Boolean> delete(@PathVariable("id")String id) ;

  /**
   * 获取Coupon详情
   *
   * @param id
   * @return
   */
  @GetMapping("/coupon/{id}")
  Resp<CouponDetailVO> get(@PathVariable("id")String id) ;

  /**
   * 获取Coupon详情列表
   *
   * @param ids
   * @return
   */
  @GetMapping("/coupon/list")
  Resp<List<CouponItemVO>> getList(List<String> ids) ;

  /**
   * 获取Coupon分页列表
   *
   * @param couponSearchVO
   * @return
   */
  @GetMapping("/coupon/page")
  Resp<PgRet<CouponItemVO>> getPage(CouponSearchVO couponSearchVO);

}