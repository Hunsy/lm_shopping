/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.api.vo;

import lombok.Data;

import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.io.Serializable;

/**
 *
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@Data
public class FeedbackVO implements Serializable {

	//columns START
	/**
	 * 用户表的用户ID
	 */
	private Integer userId;
	/**
	 * 用户名称
	 */
	@Size(max = 63)
	private String username;
	/**
	 * 手机号
	 */
	@Size(max = 20)
	private String mobile;
	/**
	 * 反馈类型
	 */
	@Size(max = 63)
	private String feedType;
	/**
	 * 反馈内容
	 */
	@Size(max = 1023)
	private String content;
	/**
	 * 状态
	 */
	private Integer status;
	/**
	 * 是否含有图片
	 */
	@NotNull
	private Boolean hasPicture;
	/**
	 * 图片地址列表，采用JSON数组格式
	 */
	@NotNull
	@Size(max = 1023)
	private String picUrls;
	/**
	 * 创建时间
	 */
	@NotNull
	private Date addTime;
	/**
	 * 更新时间
	 */
	@NotNull
	private Date updateTime;
	/**
	 * 逻辑删除
	 */
	@NotNull
	private Boolean deleted;
	//columns END
}


