/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.api.client;

import net.d2dcloud.api.vo.*;
import net.d2dcloud.core.resp.PgRet;
import net.d2dcloud.core.resp.Resp;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@FeignClient("lm-service")
public interface AddressFeignClient {

  /**
   * 新增Address
   *
   * @param addressVO
   * @return
   */
  @PostMapping("/address")
  Resp<Boolean> add(@Valid @RequestBody AddressVO addressVO);

  /**
   * 修改Address
   *
   * @param addressUpdateVO
   * @return
   */
  @PutMapping("/address")
  Resp<Boolean> update(@Valid @RequestBody AddressUpdateVO addressUpdateVO);

  /**
   * 删除Address
   *
   * @param id
   * @return
   */
  @DeleteMapping("/address/{id}")
  Resp<Boolean> delete(@PathVariable("id") String id);

  /**
   * 获取Address详情
   *
   * @param id
   * @return
   */
  @GetMapping("/address/{id}")
  Resp<AddressDetailVO> get(@PathVariable("id") String id);

  /**
   * 获取Address详情列表
   *
   * @param ids
   * @return
   */
  @GetMapping("/address/list")
  Resp<List<AddressItemVO>> getList(List<String> ids);

  /**
   * 获取Address分页列表
   *
   * @param addressSearchVO
   * @return
   */
  @GetMapping("/address/page")
  Resp<PgRet<AddressItemVO>> getPage(AddressSearchVO addressSearchVO);

}