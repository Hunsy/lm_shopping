/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.api.vo;

import lombok.Data;

import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.io.Serializable;

/**
 *
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@Data
public class CollectVO implements Serializable {

	//columns START
	/**
	 * 用户表的用户ID
	 */
	private Integer userId;
	/**
	 * 如果type=0，则是商品ID；如果type=1，则是专题ID
	 */
	private Integer valueId;
	/**
	 * 收藏类型，如果type=0，则是商品ID；如果type=1，则是专题ID
	 */
	private Integer type;
	/**
	 * 创建时间
	 */
	@NotNull
	private Date addTime;
	/**
	 * 更新时间
	 */
	@NotNull
	private Date updateTime;
	/**
	 * 逻辑删除
	 */
	@NotNull
	private Boolean deleted;
	//columns END
}


