/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.api.vo;

import lombok.Data;

import java.util.Date;

import java.io.Serializable;


/**
 *  
 *
 * @author codeGen_1.0.0
 */
@Data
public class SearchHistoryDetailVO implements Serializable {

	//columns START
	/**
	 * 
	 */
	private String id;
	/**
	 * 用户表的用户ID
	 */
	private Integer userId;
	/**
	 * 搜索关键字
	 */
	private String keyword;
	/**
	 * 搜索来源，如pc、wx、app
	 */
	private String from;
	/**
	 * 创建时间
	 */
	private Date addTime;
	/**
	 * 更新时间
	 */
	private Date updateTime;
	/**
	 * 逻辑删除
	 */
	private Boolean deleted;
	//columns END
}


