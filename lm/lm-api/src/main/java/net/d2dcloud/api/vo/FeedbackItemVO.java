/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.api.vo;

import lombok.Data;

import java.util.Date;

import java.io.Serializable;

/**
 *  
 *
 * @author codeGen_1.0.0
 */
@Data
public class FeedbackItemVO implements Serializable {

	//columns START
	/**
	 * 
	 */
	private String id;
	/**
	 * 用户表的用户ID
	 */
	private Integer userId;
	/**
	 * 用户名称
	 */
	private String username;
	/**
	 * 手机号
	 */
	private String mobile;
	/**
	 * 反馈类型
	 */
	private String feedType;
	/**
	 * 反馈内容
	 */
	private String content;
	/**
	 * 状态
	 */
	private Integer status;
	/**
	 * 是否含有图片
	 */
	private Boolean hasPicture;
	/**
	 * 图片地址列表，采用JSON数组格式
	 */
	private String picUrls;
	/**
	 * 创建时间
	 */
	private Date addTime;
	/**
	 * 更新时间
	 */
	private Date updateTime;
	/**
	 * 逻辑删除
	 */
	private Boolean deleted;
	//columns END
}


