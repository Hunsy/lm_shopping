/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.api.vo;

import lombok.Data;


import java.io.Serializable;


/**
 *  
 *
 * @author codeGen_1.0.0
 */
@Data
public class RegionDetailVO implements Serializable {

	//columns START
	/**
	 * 
	 */
	private String id;
	/**
	 * 行政区域父ID，例如区县的pid指向市，市的pid指向省，省的pid则是0
	 */
	private Integer pid;
	/**
	 * 行政区域名称
	 */
	private String name;
	/**
	 * 行政区域类型，如如1则是省， 如果是2则是市，如果是3则是区县
	 */
	private Integer type;
	/**
	 * 行政区域编码
	 */
	private Integer code;
	//columns END
}


