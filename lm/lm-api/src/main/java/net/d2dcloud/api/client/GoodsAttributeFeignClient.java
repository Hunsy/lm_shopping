/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.api.client;

import net.d2dcloud.api.resp.LmPgRet;
import net.d2dcloud.api.resp.LmResult;

import net.d2dcloud.api.vo.GoodsAttributeVO;
import net.d2dcloud.api.vo.GoodsAttributeDetailVO;
import net.d2dcloud.api.vo.GoodsAttributeUpdateVO;
import net.d2dcloud.api.vo.GoodsAttributeItemVO;
import net.d2dcloud.api.vo.GoodsAttributeSearchVO;

import org.springframework.web.bind.annotation.*;
import org.springframework.cloud.openfeign.FeignClient;

import javax.validation.Valid;

import java.util.List;

/**
 *
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@FeignClient("lm-service")
public interface GoodsAttributeFeignClient  {

  /**
   * 新增GoodsAttribute
   *
   * @param goodsAttributeVO
   * @return
   */
  @PostMapping("/goodsAttribute")
  LmResult<Boolean> add(@Valid @RequestBody GoodsAttributeVO goodsAttributeVO);

  /**
   * 修改GoodsAttribute
   *
   * @param goodsAttributeUpdateVO
   * @return
   */
  @PutMapping("/goodsAttribute")
  Resp<Boolean> update(@Valid @RequestBody GoodsAttributeUpdateVO goodsAttributeUpdateVO) ;

  /**
   * 删除GoodsAttribute
   *
   * @param id
   * @return
   */
  @DeleteMapping("/goodsAttribute/{id}")
  Resp<Boolean> delete(@PathVariable("id")String id) ;

  /**
   * 获取GoodsAttribute详情
   *
   * @param id
   * @return
   */
  @GetMapping("/goodsAttribute/{id}")
  Resp<GoodsAttributeDetailVO> get(@PathVariable("id")String id) ;

  /**
   * 获取GoodsAttribute详情列表
   *
   * @param ids
   * @return
   */
  @GetMapping("/goodsAttribute/list")
  Resp<List<GoodsAttributeItemVO>> getList(List<String> ids) ;

  /**
   * 获取GoodsAttribute分页列表
   *
   * @param goodsAttributeSearchVO
   * @return
   */
  @GetMapping("/goodsAttribute/page")
  Resp<PgRet<GoodsAttributeItemVO>> getPage(GoodsAttributeSearchVO goodsAttributeSearchVO);

}