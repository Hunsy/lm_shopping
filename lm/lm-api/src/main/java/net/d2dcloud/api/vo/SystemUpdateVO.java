/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.api.vo;

import lombok.Data;

import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.io.Serializable;

/**
 *  
 *
 * @author codeGen_1.0.0
 */
@Data
public class SystemUpdateVO implements Serializable {

	//columns START
	@NotNull
	private String id;
	/**
	 * 系统配置名
	 */
	@Size(max = 255)
	private String keyName;
	/**
	 * 系统配置值
	 */
	@Size(max = 255)
	private String keyValue;
	/**
	 * 创建时间
	 */
	private Date addTime;
	/**
	 * 更新时间
	 */
	private Date updateTime;
	/**
	 * 逻辑删除
	 */
	private Boolean deleted;
	//columns END
}


