/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.api.vo;

import lombok.Data;

import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.io.Serializable;

/**
 *
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@Data
public class CouponUserVO implements Serializable {

	//columns START
	/**
	 * 用户ID
	 */
	private Integer userId;
	/**
	 * 优惠券ID
	 */
	private Integer couponId;
	/**
	 * 使用状态, 如果是0则未使用；如果是1则已使用；如果是2则已过期；如果是3则已经下架；
	 */
	@NotNull
	private Integer status;
	/**
	 * 使用时间
	 */
	@NotNull
	private Date usedTime;
	/**
	 * 有效期开始时间
	 */
	@NotNull
	private Date startTime;
	/**
	 * 有效期截至时间
	 */
	@NotNull
	private Date endTime;
	/**
	 * 订单ID
	 */
	@NotNull
	private Integer orderId;
	/**
	 * 创建时间
	 */
	@NotNull
	private Date addTime;
	/**
	 * 更新时间
	 */
	@NotNull
	private Date updateTime;
	/**
	 * 逻辑删除
	 */
	@NotNull
	private Boolean deleted;
	//columns END
}


