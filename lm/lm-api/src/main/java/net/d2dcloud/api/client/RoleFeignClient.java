/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.api.client;

import net.d2dcloud.api.resp.LmPgRet;
import net.d2dcloud.api.resp.LmResult;

import net.d2dcloud.api.vo.RoleVO;
import net.d2dcloud.api.vo.RoleDetailVO;
import net.d2dcloud.api.vo.RoleUpdateVO;
import net.d2dcloud.api.vo.RoleItemVO;
import net.d2dcloud.api.vo.RoleSearchVO;

import org.springframework.web.bind.annotation.*;
import org.springframework.cloud.openfeign.FeignClient;

import javax.validation.Valid;

import java.util.List;

/**
 *
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@FeignClient("lm-service")
public interface RoleFeignClient  {

  /**
   * 新增Role
   *
   * @param roleVO
   * @return
   */
  @PostMapping("/role")
  LmResult<Boolean> add(@Valid @RequestBody RoleVO roleVO);

  /**
   * 修改Role
   *
   * @param roleUpdateVO
   * @return
   */
  @PutMapping("/role")
  Resp<Boolean> update(@Valid @RequestBody RoleUpdateVO roleUpdateVO) ;

  /**
   * 删除Role
   *
   * @param id
   * @return
   */
  @DeleteMapping("/role/{id}")
  Resp<Boolean> delete(@PathVariable("id")String id) ;

  /**
   * 获取Role详情
   *
   * @param id
   * @return
   */
  @GetMapping("/role/{id}")
  Resp<RoleDetailVO> get(@PathVariable("id")String id) ;

  /**
   * 获取Role详情列表
   *
   * @param ids
   * @return
   */
  @GetMapping("/role/list")
  Resp<List<RoleItemVO>> getList(List<String> ids) ;

  /**
   * 获取Role分页列表
   *
   * @param roleSearchVO
   * @return
   */
  @GetMapping("/role/page")
  Resp<PgRet<RoleItemVO>> getPage(RoleSearchVO roleSearchVO);

}