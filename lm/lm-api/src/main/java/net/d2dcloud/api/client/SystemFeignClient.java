/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.api.client;

import net.d2dcloud.api.resp.LmPgRet;
import net.d2dcloud.api.resp.LmResult;

import net.d2dcloud.api.vo.SystemVO;
import net.d2dcloud.api.vo.SystemDetailVO;
import net.d2dcloud.api.vo.SystemUpdateVO;
import net.d2dcloud.api.vo.SystemItemVO;
import net.d2dcloud.api.vo.SystemSearchVO;

import org.springframework.web.bind.annotation.*;
import org.springframework.cloud.openfeign.FeignClient;

import javax.validation.Valid;

import java.util.List;

/**
 *
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@FeignClient("lm-service")
public interface SystemFeignClient  {

  /**
   * 新增System
   *
   * @param systemVO
   * @return
   */
  @PostMapping("/system")
  LmResult<Boolean> add(@Valid @RequestBody SystemVO systemVO);

  /**
   * 修改System
   *
   * @param systemUpdateVO
   * @return
   */
  @PutMapping("/system")
  Resp<Boolean> update(@Valid @RequestBody SystemUpdateVO systemUpdateVO) ;

  /**
   * 删除System
   *
   * @param id
   * @return
   */
  @DeleteMapping("/system/{id}")
  Resp<Boolean> delete(@PathVariable("id")String id) ;

  /**
   * 获取System详情
   *
   * @param id
   * @return
   */
  @GetMapping("/system/{id}")
  Resp<SystemDetailVO> get(@PathVariable("id")String id) ;

  /**
   * 获取System详情列表
   *
   * @param ids
   * @return
   */
  @GetMapping("/system/list")
  Resp<List<SystemItemVO>> getList(List<String> ids) ;

  /**
   * 获取System分页列表
   *
   * @param systemSearchVO
   * @return
   */
  @GetMapping("/system/page")
  Resp<PgRet<SystemItemVO>> getPage(SystemSearchVO systemSearchVO);

}