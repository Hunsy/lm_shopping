/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.api.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

import net.d2dcloud.resp.CategoryPgInVo;

/**
 *  
 *
 * @author codeGen_1.0.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class CategorySearchVO extends CategoryPgInVo {

	//columns START
	/**
	 * 
	 */
	private String id;
	/**
	 * 类目名称
	 */
	private String name;
	/**
	 * 类目关键字，以JSON数组格式
	 */
	private String keywords;
	/**
	 * 类目广告语介绍
	 */
	private String desc;
	/**
	 * 父类目ID
	 */
	private Integer pid;
	/**
	 * 类目图标
	 */
	private String iconUrl;
	/**
	 * 类目图片
	 */
	private String picUrl;
	/**
	 * 
	 */
	private String level;
	/**
	 * 排序
	 */
	private Integer sortOrder;
	/**
	 * 创建时间
	 */
	private Date addTime;
	/**
	 * 更新时间
	 */
	private Date updateTime;
	/**
	 * 逻辑删除
	 */
	private Boolean deleted;
	//columns END
}


