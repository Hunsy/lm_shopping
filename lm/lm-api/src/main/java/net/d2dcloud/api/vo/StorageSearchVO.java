/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.api.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

import net.d2dcloud.resp.StoragePgInVo;

/**
 *  
 *
 * @author codeGen_1.0.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class StorageSearchVO extends StoragePgInVo {

	//columns START
	/**
	 * 
	 */
	private String id;
	/**
	 * 文件的唯一索引
	 */
	private String key;
	/**
	 * 文件名
	 */
	private String name;
	/**
	 * 文件类型
	 */
	private String type;
	/**
	 * 文件大小
	 */
	private Integer size;
	/**
	 * 文件访问链接
	 */
	private String url;
	/**
	 * 创建时间
	 */
	private Date addTime;
	/**
	 * 更新时间
	 */
	private Date updateTime;
	/**
	 * 逻辑删除
	 */
	private Boolean deleted;
	//columns END
}


