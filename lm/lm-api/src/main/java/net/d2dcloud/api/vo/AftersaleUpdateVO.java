/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.api.vo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.io.Serializable;

/**
 *  
 *
 * @author codeGen_1.0.0
 */
@Data
public class AftersaleUpdateVO implements Serializable {

	//columns START
	@NotNull
	private String id;
	/**
	 * 售后编号
	 */
	@Size(max = 63)
	private String aftersaleSn;
	/**
	 * 订单ID
	 */
	private Integer orderId;
	/**
	 * 用户ID
	 */
	private Integer userId;
	/**
	 * 售后类型，0是未收货退款，1是已收货（无需退货）退款，2用户退货退款
	 */
	private Integer type;
	/**
	 * 退款原因
	 */
	@Size(max = 31)
	private String reason;
	/**
	 * 退款金额
	 */
	private BigDecimal amount;
	/**
	 * 退款凭证图片链接数组
	 */
	@Size(max = 1023)
	private String pictures;
	/**
	 * 退款说明
	 */
	@Size(max = 511)
	private String comment;
	/**
	 * 售后状态，0是可申请，1是用户已申请，2是管理员审核通过，3是管理员退款成功，4是管理员审核拒绝，5是用户已取消
	 */
	private Integer status;
	/**
	 * 管理员操作时间
	 */
	private Date handleTime;
	/**
	 * 添加时间
	 */
	private Date addTime;
	/**
	 * 更新时间
	 */
	private Date updateTime;
	/**
	 * 售后编号
	 */
	private Boolean deleted;
	//columns END
}


