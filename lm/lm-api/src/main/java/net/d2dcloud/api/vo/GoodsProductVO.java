/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.api.vo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.io.Serializable;

/**
 *
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@Data
public class GoodsProductVO implements Serializable {

	//columns START
	/**
	 * 商品表的商品ID
	 */
	private Integer goodsId;
	/**
	 * 商品规格值列表，采用JSON数组格式
	 */
	@Size(max = 1023)
	private String specifications;
	/**
	 * 商品货品价格
	 */
	private BigDecimal price;
	/**
	 * 商品货品数量
	 */
	private Integer number;
	/**
	 * 商品货品图片
	 */
	@NotNull
	@Size(max = 125)
	private String url;
	/**
	 * 创建时间
	 */
	@NotNull
	private Date addTime;
	/**
	 * 更新时间
	 */
	@NotNull
	private Date updateTime;
	/**
	 * 逻辑删除
	 */
	@NotNull
	private Boolean deleted;
	//columns END
}


