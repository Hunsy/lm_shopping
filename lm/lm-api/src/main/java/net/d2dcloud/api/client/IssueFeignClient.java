/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.api.client;

import net.d2dcloud.api.resp.LmPgRet;
import net.d2dcloud.api.resp.LmResult;

import net.d2dcloud.api.vo.IssueVO;
import net.d2dcloud.api.vo.IssueDetailVO;
import net.d2dcloud.api.vo.IssueUpdateVO;
import net.d2dcloud.api.vo.IssueItemVO;
import net.d2dcloud.api.vo.IssueSearchVO;

import org.springframework.web.bind.annotation.*;
import org.springframework.cloud.openfeign.FeignClient;

import javax.validation.Valid;

import java.util.List;

/**
 *
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@FeignClient("lm-service")
public interface IssueFeignClient  {

  /**
   * 新增Issue
   *
   * @param issueVO
   * @return
   */
  @PostMapping("/issue")
  LmResult<Boolean> add(@Valid @RequestBody IssueVO issueVO);

  /**
   * 修改Issue
   *
   * @param issueUpdateVO
   * @return
   */
  @PutMapping("/issue")
  Resp<Boolean> update(@Valid @RequestBody IssueUpdateVO issueUpdateVO) ;

  /**
   * 删除Issue
   *
   * @param id
   * @return
   */
  @DeleteMapping("/issue/{id}")
  Resp<Boolean> delete(@PathVariable("id")String id) ;

  /**
   * 获取Issue详情
   *
   * @param id
   * @return
   */
  @GetMapping("/issue/{id}")
  Resp<IssueDetailVO> get(@PathVariable("id")String id) ;

  /**
   * 获取Issue详情列表
   *
   * @param ids
   * @return
   */
  @GetMapping("/issue/list")
  Resp<List<IssueItemVO>> getList(List<String> ids) ;

  /**
   * 获取Issue分页列表
   *
   * @param issueSearchVO
   * @return
   */
  @GetMapping("/issue/page")
  Resp<PgRet<IssueItemVO>> getPage(IssueSearchVO issueSearchVO);

}