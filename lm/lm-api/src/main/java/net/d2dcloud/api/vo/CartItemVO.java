/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.api.vo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

import java.io.Serializable;

/**
 *  
 *
 * @author codeGen_1.0.0
 */
@Data
public class CartItemVO implements Serializable {

	//columns START
	/**
	 * 
	 */
	private String id;
	/**
	 * 用户表的用户ID
	 */
	private Integer userId;
	/**
	 * 商品表的商品ID
	 */
	private Integer goodsId;
	/**
	 * 商品编号
	 */
	private String goodsSn;
	/**
	 * 商品名称
	 */
	private String goodsName;
	/**
	 * 商品货品表的货品ID
	 */
	private Integer productId;
	/**
	 * 商品货品的价格
	 */
	private BigDecimal price;
	/**
	 * 商品货品的数量
	 */
	private Integer number;
	/**
	 * 商品规格值列表，采用JSON数组格式
	 */
	private String specifications;
	/**
	 * 购物车中商品是否选择状态
	 */
	private Boolean checked;
	/**
	 * 商品图片或者商品货品图片
	 */
	private String picUrl;
	/**
	 * 创建时间
	 */
	private Date addTime;
	/**
	 * 更新时间
	 */
	private Date updateTime;
	/**
	 * 逻辑删除
	 */
	private Boolean deleted;
	//columns END
}


