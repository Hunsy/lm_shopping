/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.api.vo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.io.Serializable;

/**
 *
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@Data
public class CartVO implements Serializable {

	//columns START
	/**
	 * 用户表的用户ID
	 */
	@NotNull
	private Integer userId;
	/**
	 * 商品表的商品ID
	 */
	@NotNull
	private Integer goodsId;
	/**
	 * 商品编号
	 */
	@NotNull
	@Size(max = 63)
	private String goodsSn;
	/**
	 * 商品名称
	 */
	@NotNull
	@Size(max = 127)
	private String goodsName;
	/**
	 * 商品货品表的货品ID
	 */
	@NotNull
	private Integer productId;
	/**
	 * 商品货品的价格
	 */
	@NotNull
	private BigDecimal price;
	/**
	 * 商品货品的数量
	 */
	@NotNull
	private Integer number;
	/**
	 * 商品规格值列表，采用JSON数组格式
	 */
	@NotNull
	@Size(max = 1023)
	private String specifications;
	/**
	 * 购物车中商品是否选择状态
	 */
	@NotNull
	private Boolean checked;
	/**
	 * 商品图片或者商品货品图片
	 */
	@NotNull
	@Size(max = 255)
	private String picUrl;
	/**
	 * 创建时间
	 */
	@NotNull
	private Date addTime;
	/**
	 * 更新时间
	 */
	@NotNull
	private Date updateTime;
	/**
	 * 逻辑删除
	 */
	@NotNull
	private Boolean deleted;
	//columns END
}


