/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.api.vo;

import lombok.Data;

import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.io.Serializable;

/**
 *
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@Data
public class PermissionVO implements Serializable {

	//columns START
	/**
	 * 角色ID
	 */
	@NotNull
	private Integer roleId;
	/**
	 * 权限
	 */
	@NotNull
	@Size(max = 63)
	private String permission;
	/**
	 * 创建时间
	 */
	@NotNull
	private Date addTime;
	/**
	 * 更新时间
	 */
	@NotNull
	private Date updateTime;
	/**
	 * 逻辑删除
	 */
	@NotNull
	private Boolean deleted;
	//columns END
}


