/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.api.vo;

import lombok.Data;

import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.io.Serializable;

/**
 *
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@Data
public class UserVO implements Serializable {

	//columns START
	/**
	 * 用户名称
	 */
	@Size(max = 63)
	private String username;
	/**
	 * 用户密码
	 */
	@Size(max = 63)
	private String password;
	/**
	 * 性别：0 未知， 1男， 1 女
	 */
	private Integer gender;
	/**
	 * 生日
	 */
	@NotNull
	private Date birthday;
	/**
	 * 最近一次登录时间
	 */
	@NotNull
	private Date lastLoginTime;
	/**
	 * 最近一次登录IP地址
	 */
	@Size(max = 63)
	private String lastLoginIp;
	/**
	 * 0 普通用户，1 VIP用户，2 高级VIP用户
	 */
	@NotNull
	private Integer userLevel;
	/**
	 * 用户昵称或网络名称
	 */
	@Size(max = 63)
	private String nickname;
	/**
	 * 用户手机号码
	 */
	@Size(max = 20)
	private String mobile;
	/**
	 * 用户头像图片
	 */
	@Size(max = 255)
	private String avatar;
	/**
	 * 微信登录openid
	 */
	@Size(max = 63)
	private String weixinOpenid;
	/**
	 * 微信登录会话KEY
	 */
	@Size(max = 100)
	private String sessionKey;
	/**
	 * 0 可用, 1 禁用, 2 注销
	 */
	private Integer status;
	/**
	 * 创建时间
	 */
	@NotNull
	private Date addTime;
	/**
	 * 更新时间
	 */
	@NotNull
	private Date updateTime;
	/**
	 * 逻辑删除
	 */
	@NotNull
	private Boolean deleted;
	//columns END
}


