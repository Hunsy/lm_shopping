/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.api.client;

import net.d2dcloud.api.resp.LmPgRet;
import net.d2dcloud.api.resp.LmResult;

import net.d2dcloud.api.vo.GoodsProductVO;
import net.d2dcloud.api.vo.GoodsProductDetailVO;
import net.d2dcloud.api.vo.GoodsProductUpdateVO;
import net.d2dcloud.api.vo.GoodsProductItemVO;
import net.d2dcloud.api.vo.GoodsProductSearchVO;

import org.springframework.web.bind.annotation.*;
import org.springframework.cloud.openfeign.FeignClient;

import javax.validation.Valid;

import java.util.List;

/**
 *
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@FeignClient("lm-service")
public interface GoodsProductFeignClient  {

  /**
   * 新增GoodsProduct
   *
   * @param goodsProductVO
   * @return
   */
  @PostMapping("/goodsProduct")
  LmResult<Boolean> add(@Valid @RequestBody GoodsProductVO goodsProductVO);

  /**
   * 修改GoodsProduct
   *
   * @param goodsProductUpdateVO
   * @return
   */
  @PutMapping("/goodsProduct")
  Resp<Boolean> update(@Valid @RequestBody GoodsProductUpdateVO goodsProductUpdateVO) ;

  /**
   * 删除GoodsProduct
   *
   * @param id
   * @return
   */
  @DeleteMapping("/goodsProduct/{id}")
  Resp<Boolean> delete(@PathVariable("id")String id) ;

  /**
   * 获取GoodsProduct详情
   *
   * @param id
   * @return
   */
  @GetMapping("/goodsProduct/{id}")
  Resp<GoodsProductDetailVO> get(@PathVariable("id")String id) ;

  /**
   * 获取GoodsProduct详情列表
   *
   * @param ids
   * @return
   */
  @GetMapping("/goodsProduct/list")
  Resp<List<GoodsProductItemVO>> getList(List<String> ids) ;

  /**
   * 获取GoodsProduct分页列表
   *
   * @param goodsProductSearchVO
   * @return
   */
  @GetMapping("/goodsProduct/page")
  Resp<PgRet<GoodsProductItemVO>> getPage(GoodsProductSearchVO goodsProductSearchVO);

}