/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.api.vo;

import lombok.Data;

import java.util.Date;

import java.io.Serializable;


/**
 *  
 *
 * @author codeGen_1.0.0
 */
@Data
public class CommentDetailVO implements Serializable {

	//columns START
	/**
	 * 
	 */
	private String id;
	/**
	 * 如果type=0，则是商品评论；如果是type=1，则是专题评论。
	 */
	private Integer valueId;
	/**
	 * 评论类型，如果type=0，则是商品评论；如果是type=1，则是专题评论；
	 */
	private Integer type;
	/**
	 * 评论内容
	 */
	private String content;
	/**
	 * 管理员回复内容
	 */
	private String adminContent;
	/**
	 * 用户表的用户ID
	 */
	private Integer userId;
	/**
	 * 是否含有图片
	 */
	private Boolean hasPicture;
	/**
	 * 图片地址列表，采用JSON数组格式
	 */
	private String picUrls;
	/**
	 * 评分， 1-5
	 */
	private Integer star;
	/**
	 * 创建时间
	 */
	private Date addTime;
	/**
	 * 更新时间
	 */
	private Date updateTime;
	/**
	 * 逻辑删除
	 */
	private Boolean deleted;
	//columns END
}


