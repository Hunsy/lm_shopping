/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.api.client;

import net.d2dcloud.api.resp.LmPgRet;
import net.d2dcloud.api.resp.LmResult;

import net.d2dcloud.api.vo.CouponUserVO;
import net.d2dcloud.api.vo.CouponUserDetailVO;
import net.d2dcloud.api.vo.CouponUserUpdateVO;
import net.d2dcloud.api.vo.CouponUserItemVO;
import net.d2dcloud.api.vo.CouponUserSearchVO;

import org.springframework.web.bind.annotation.*;
import org.springframework.cloud.openfeign.FeignClient;

import javax.validation.Valid;

import java.util.List;

/**
 *
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@FeignClient("lm-service")
public interface CouponUserFeignClient  {

  /**
   * 新增CouponUser
   *
   * @param couponUserVO
   * @return
   */
  @PostMapping("/couponUser")
  LmResult<Boolean> add(@Valid @RequestBody CouponUserVO couponUserVO);

  /**
   * 修改CouponUser
   *
   * @param couponUserUpdateVO
   * @return
   */
  @PutMapping("/couponUser")
  Resp<Boolean> update(@Valid @RequestBody CouponUserUpdateVO couponUserUpdateVO) ;

  /**
   * 删除CouponUser
   *
   * @param id
   * @return
   */
  @DeleteMapping("/couponUser/{id}")
  Resp<Boolean> delete(@PathVariable("id")String id) ;

  /**
   * 获取CouponUser详情
   *
   * @param id
   * @return
   */
  @GetMapping("/couponUser/{id}")
  Resp<CouponUserDetailVO> get(@PathVariable("id")String id) ;

  /**
   * 获取CouponUser详情列表
   *
   * @param ids
   * @return
   */
  @GetMapping("/couponUser/list")
  Resp<List<CouponUserItemVO>> getList(List<String> ids) ;

  /**
   * 获取CouponUser分页列表
   *
   * @param couponUserSearchVO
   * @return
   */
  @GetMapping("/couponUser/page")
  Resp<PgRet<CouponUserItemVO>> getPage(CouponUserSearchVO couponUserSearchVO);

}