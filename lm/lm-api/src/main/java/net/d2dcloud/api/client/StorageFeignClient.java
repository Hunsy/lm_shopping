/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.api.client;

import net.d2dcloud.api.resp.LmPgRet;
import net.d2dcloud.api.resp.LmResult;

import net.d2dcloud.api.vo.StorageVO;
import net.d2dcloud.api.vo.StorageDetailVO;
import net.d2dcloud.api.vo.StorageUpdateVO;
import net.d2dcloud.api.vo.StorageItemVO;
import net.d2dcloud.api.vo.StorageSearchVO;

import org.springframework.web.bind.annotation.*;
import org.springframework.cloud.openfeign.FeignClient;

import javax.validation.Valid;

import java.util.List;

/**
 *
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@FeignClient("lm-service")
public interface StorageFeignClient  {

  /**
   * 新增Storage
   *
   * @param storageVO
   * @return
   */
  @PostMapping("/storage")
  LmResult<Boolean> add(@Valid @RequestBody StorageVO storageVO);

  /**
   * 修改Storage
   *
   * @param storageUpdateVO
   * @return
   */
  @PutMapping("/storage")
  Resp<Boolean> update(@Valid @RequestBody StorageUpdateVO storageUpdateVO) ;

  /**
   * 删除Storage
   *
   * @param id
   * @return
   */
  @DeleteMapping("/storage/{id}")
  Resp<Boolean> delete(@PathVariable("id")String id) ;

  /**
   * 获取Storage详情
   *
   * @param id
   * @return
   */
  @GetMapping("/storage/{id}")
  Resp<StorageDetailVO> get(@PathVariable("id")String id) ;

  /**
   * 获取Storage详情列表
   *
   * @param ids
   * @return
   */
  @GetMapping("/storage/list")
  Resp<List<StorageItemVO>> getList(List<String> ids) ;

  /**
   * 获取Storage分页列表
   *
   * @param storageSearchVO
   * @return
   */
  @GetMapping("/storage/page")
  Resp<PgRet<StorageItemVO>> getPage(StorageSearchVO storageSearchVO);

}