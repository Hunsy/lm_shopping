/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.api.client;

import net.d2dcloud.api.vo.*;
import net.d2dcloud.core.resp.PgRet;
import net.d2dcloud.core.resp.Resp;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@FeignClient("lm-service")
public interface AdFeignClient {

  /**
   * 新增Ad
   *
   * @param adVO
   * @return
   */
  @PostMapping("/ad")
  Resp<Boolean> add(@Valid @RequestBody AdVO adVO);

  /**
   * 修改Ad
   *
   * @param adUpdateVO
   * @return
   */
  @PutMapping("/ad")
  Resp<Boolean> update(@Valid @RequestBody AdUpdateVO adUpdateVO);

  /**
   * 删除Ad
   *
   * @param id
   * @return
   */
  @DeleteMapping("/ad/{id}")
  Resp<Boolean> delete(@PathVariable("id") String id);

  /**
   * 获取Ad详情
   *
   * @param id
   * @return
   */
  @GetMapping("/ad/{id}")
  Resp<AdDetailVO> get(@PathVariable("id") String id);

  /**
   * 获取Ad详情列表
   *
   * @param ids
   * @return
   */
  @GetMapping("/ad/list")
  Resp<List<AdItemVO>> getList(List<String> ids);

  /**
   * 获取Ad分页列表
   *
   * @param adSearchVO
   * @return
   */
  @GetMapping("/ad/page")
  Resp<PgRet<AdItemVO>> getPage(AdSearchVO adSearchVO);

}