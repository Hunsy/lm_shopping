/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.api.vo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.io.Serializable;

/**
 *
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@Data
public class OrderGoodsVO implements Serializable {

	//columns START
	/**
	 * 订单表的订单ID
	 */
	private Integer orderId;
	/**
	 * 商品表的商品ID
	 */
	private Integer goodsId;
	/**
	 * 商品名称
	 */
	@Size(max = 127)
	private String goodsName;
	/**
	 * 商品编号
	 */
	@Size(max = 63)
	private String goodsSn;
	/**
	 * 商品货品表的货品ID
	 */
	private Integer productId;
	/**
	 * 商品货品的购买数量
	 */
	private Integer number;
	/**
	 * 商品货品的售价
	 */
	private BigDecimal price;
	/**
	 * 商品货品的规格列表
	 */
	@Size(max = 1023)
	private String specifications;
	/**
	 * 商品货品图片或者商品图片
	 */
	@Size(max = 255)
	private String picUrl;
	/**
	 * 订单商品评论，如果是-1，则超期不能评价；如果是0，则可以评价；如果其他值，则是comment表里面的评论ID。
	 */
	@NotNull
	private Integer comment;
	/**
	 * 创建时间
	 */
	@NotNull
	private Date addTime;
	/**
	 * 更新时间
	 */
	@NotNull
	private Date updateTime;
	/**
	 * 逻辑删除
	 */
	@NotNull
	private Boolean deleted;
	//columns END
}


