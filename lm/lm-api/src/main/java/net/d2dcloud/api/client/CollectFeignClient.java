/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.api.client;

import net.d2dcloud.api.resp.LmPgRet;
import net.d2dcloud.api.resp.LmResult;

import net.d2dcloud.api.vo.CollectVO;
import net.d2dcloud.api.vo.CollectDetailVO;
import net.d2dcloud.api.vo.CollectUpdateVO;
import net.d2dcloud.api.vo.CollectItemVO;
import net.d2dcloud.api.vo.CollectSearchVO;

import org.springframework.web.bind.annotation.*;
import org.springframework.cloud.openfeign.FeignClient;

import javax.validation.Valid;

import java.util.List;

/**
 *
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@FeignClient("lm-service")
public interface CollectFeignClient  {

  /**
   * 新增Collect
   *
   * @param collectVO
   * @return
   */
  @PostMapping("/collect")
  LmResult<Boolean> add(@Valid @RequestBody CollectVO collectVO);

  /**
   * 修改Collect
   *
   * @param collectUpdateVO
   * @return
   */
  @PutMapping("/collect")
  Resp<Boolean> update(@Valid @RequestBody CollectUpdateVO collectUpdateVO) ;

  /**
   * 删除Collect
   *
   * @param id
   * @return
   */
  @DeleteMapping("/collect/{id}")
  Resp<Boolean> delete(@PathVariable("id")String id) ;

  /**
   * 获取Collect详情
   *
   * @param id
   * @return
   */
  @GetMapping("/collect/{id}")
  Resp<CollectDetailVO> get(@PathVariable("id")String id) ;

  /**
   * 获取Collect详情列表
   *
   * @param ids
   * @return
   */
  @GetMapping("/collect/list")
  Resp<List<CollectItemVO>> getList(List<String> ids) ;

  /**
   * 获取Collect分页列表
   *
   * @param collectSearchVO
   * @return
   */
  @GetMapping("/collect/page")
  Resp<PgRet<CollectItemVO>> getPage(CollectSearchVO collectSearchVO);

}