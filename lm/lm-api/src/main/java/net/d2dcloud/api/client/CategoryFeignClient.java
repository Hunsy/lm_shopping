/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.api.client;

import net.d2dcloud.api.resp.LmPgRet;
import net.d2dcloud.api.resp.LmResult;

import net.d2dcloud.api.vo.CategoryVO;
import net.d2dcloud.api.vo.CategoryDetailVO;
import net.d2dcloud.api.vo.CategoryUpdateVO;
import net.d2dcloud.api.vo.CategoryItemVO;
import net.d2dcloud.api.vo.CategorySearchVO;

import org.springframework.web.bind.annotation.*;
import org.springframework.cloud.openfeign.FeignClient;

import javax.validation.Valid;

import java.util.List;

/**
 *
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@FeignClient("lm-service")
public interface CategoryFeignClient  {

  /**
   * 新增Category
   *
   * @param categoryVO
   * @return
   */
  @PostMapping("/category")
  LmResult<Boolean> add(@Valid @RequestBody CategoryVO categoryVO);

  /**
   * 修改Category
   *
   * @param categoryUpdateVO
   * @return
   */
  @PutMapping("/category")
  Resp<Boolean> update(@Valid @RequestBody CategoryUpdateVO categoryUpdateVO) ;

  /**
   * 删除Category
   *
   * @param id
   * @return
   */
  @DeleteMapping("/category/{id}")
  Resp<Boolean> delete(@PathVariable("id")String id) ;

  /**
   * 获取Category详情
   *
   * @param id
   * @return
   */
  @GetMapping("/category/{id}")
  Resp<CategoryDetailVO> get(@PathVariable("id")String id) ;

  /**
   * 获取Category详情列表
   *
   * @param ids
   * @return
   */
  @GetMapping("/category/list")
  Resp<List<CategoryItemVO>> getList(List<String> ids) ;

  /**
   * 获取Category分页列表
   *
   * @param categorySearchVO
   * @return
   */
  @GetMapping("/category/page")
  Resp<PgRet<CategoryItemVO>> getPage(CategorySearchVO categorySearchVO);

}