/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.api.client;

import net.d2dcloud.api.resp.LmPgRet;
import net.d2dcloud.api.resp.LmResult;

import net.d2dcloud.api.vo.GrouponVO;
import net.d2dcloud.api.vo.GrouponDetailVO;
import net.d2dcloud.api.vo.GrouponUpdateVO;
import net.d2dcloud.api.vo.GrouponItemVO;
import net.d2dcloud.api.vo.GrouponSearchVO;

import org.springframework.web.bind.annotation.*;
import org.springframework.cloud.openfeign.FeignClient;

import javax.validation.Valid;

import java.util.List;

/**
 *
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@FeignClient("lm-service")
public interface GrouponFeignClient  {

  /**
   * 新增Groupon
   *
   * @param grouponVO
   * @return
   */
  @PostMapping("/groupon")
  LmResult<Boolean> add(@Valid @RequestBody GrouponVO grouponVO);

  /**
   * 修改Groupon
   *
   * @param grouponUpdateVO
   * @return
   */
  @PutMapping("/groupon")
  Resp<Boolean> update(@Valid @RequestBody GrouponUpdateVO grouponUpdateVO) ;

  /**
   * 删除Groupon
   *
   * @param id
   * @return
   */
  @DeleteMapping("/groupon/{id}")
  Resp<Boolean> delete(@PathVariable("id")String id) ;

  /**
   * 获取Groupon详情
   *
   * @param id
   * @return
   */
  @GetMapping("/groupon/{id}")
  Resp<GrouponDetailVO> get(@PathVariable("id")String id) ;

  /**
   * 获取Groupon详情列表
   *
   * @param ids
   * @return
   */
  @GetMapping("/groupon/list")
  Resp<List<GrouponItemVO>> getList(List<String> ids) ;

  /**
   * 获取Groupon分页列表
   *
   * @param grouponSearchVO
   * @return
   */
  @GetMapping("/groupon/page")
  Resp<PgRet<GrouponItemVO>> getPage(GrouponSearchVO grouponSearchVO);

}