/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.api.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

import net.d2dcloud.resp.CollectPgInVo;

/**
 *  
 *
 * @author codeGen_1.0.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class CollectSearchVO extends CollectPgInVo {

	//columns START
	/**
	 * 
	 */
	private String id;
	/**
	 * 用户表的用户ID
	 */
	private Integer userId;
	/**
	 * 如果type=0，则是商品ID；如果type=1，则是专题ID
	 */
	private Integer valueId;
	/**
	 * 收藏类型，如果type=0，则是商品ID；如果type=1，则是专题ID
	 */
	private Integer type;
	/**
	 * 创建时间
	 */
	private Date addTime;
	/**
	 * 更新时间
	 */
	private Date updateTime;
	/**
	 * 逻辑删除
	 */
	private Boolean deleted;
	//columns END
}


