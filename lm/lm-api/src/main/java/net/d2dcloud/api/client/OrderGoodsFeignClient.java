/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.api.client;

import net.d2dcloud.api.resp.LmPgRet;
import net.d2dcloud.api.resp.LmResult;

import net.d2dcloud.api.vo.OrderGoodsVO;
import net.d2dcloud.api.vo.OrderGoodsDetailVO;
import net.d2dcloud.api.vo.OrderGoodsUpdateVO;
import net.d2dcloud.api.vo.OrderGoodsItemVO;
import net.d2dcloud.api.vo.OrderGoodsSearchVO;

import org.springframework.web.bind.annotation.*;
import org.springframework.cloud.openfeign.FeignClient;

import javax.validation.Valid;

import java.util.List;

/**
 *
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@FeignClient("lm-service")
public interface OrderGoodsFeignClient  {

  /**
   * 新增OrderGoods
   *
   * @param orderGoodsVO
   * @return
   */
  @PostMapping("/orderGoods")
  LmResult<Boolean> add(@Valid @RequestBody OrderGoodsVO orderGoodsVO);

  /**
   * 修改OrderGoods
   *
   * @param orderGoodsUpdateVO
   * @return
   */
  @PutMapping("/orderGoods")
  Resp<Boolean> update(@Valid @RequestBody OrderGoodsUpdateVO orderGoodsUpdateVO) ;

  /**
   * 删除OrderGoods
   *
   * @param id
   * @return
   */
  @DeleteMapping("/orderGoods/{id}")
  Resp<Boolean> delete(@PathVariable("id")String id) ;

  /**
   * 获取OrderGoods详情
   *
   * @param id
   * @return
   */
  @GetMapping("/orderGoods/{id}")
  Resp<OrderGoodsDetailVO> get(@PathVariable("id")String id) ;

  /**
   * 获取OrderGoods详情列表
   *
   * @param ids
   * @return
   */
  @GetMapping("/orderGoods/list")
  Resp<List<OrderGoodsItemVO>> getList(List<String> ids) ;

  /**
   * 获取OrderGoods分页列表
   *
   * @param orderGoodsSearchVO
   * @return
   */
  @GetMapping("/orderGoods/page")
  Resp<PgRet<OrderGoodsItemVO>> getPage(OrderGoodsSearchVO orderGoodsSearchVO);

}