/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.api.client;

import net.d2dcloud.api.resp.LmPgRet;
import net.d2dcloud.api.resp.LmResult;

import net.d2dcloud.api.vo.UserVO;
import net.d2dcloud.api.vo.UserDetailVO;
import net.d2dcloud.api.vo.UserUpdateVO;
import net.d2dcloud.api.vo.UserItemVO;
import net.d2dcloud.api.vo.UserSearchVO;

import org.springframework.web.bind.annotation.*;
import org.springframework.cloud.openfeign.FeignClient;

import javax.validation.Valid;

import java.util.List;

/**
 *
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@FeignClient("lm-service")
public interface UserFeignClient  {

  /**
   * 新增User
   *
   * @param userVO
   * @return
   */
  @PostMapping("/user")
  LmResult<Boolean> add(@Valid @RequestBody UserVO userVO);

  /**
   * 修改User
   *
   * @param userUpdateVO
   * @return
   */
  @PutMapping("/user")
  Resp<Boolean> update(@Valid @RequestBody UserUpdateVO userUpdateVO) ;

  /**
   * 删除User
   *
   * @param id
   * @return
   */
  @DeleteMapping("/user/{id}")
  Resp<Boolean> delete(@PathVariable("id")String id) ;

  /**
   * 获取User详情
   *
   * @param id
   * @return
   */
  @GetMapping("/user/{id}")
  Resp<UserDetailVO> get(@PathVariable("id")String id) ;

  /**
   * 获取User详情列表
   *
   * @param ids
   * @return
   */
  @GetMapping("/user/list")
  Resp<List<UserItemVO>> getList(List<String> ids) ;

  /**
   * 获取User分页列表
   *
   * @param userSearchVO
   * @return
   */
  @GetMapping("/user/page")
  Resp<PgRet<UserItemVO>> getPage(UserSearchVO userSearchVO);

}