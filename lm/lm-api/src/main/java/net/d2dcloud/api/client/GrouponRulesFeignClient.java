/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.api.client;

import net.d2dcloud.api.resp.LmPgRet;
import net.d2dcloud.api.resp.LmResult;

import net.d2dcloud.api.vo.GrouponRulesVO;
import net.d2dcloud.api.vo.GrouponRulesDetailVO;
import net.d2dcloud.api.vo.GrouponRulesUpdateVO;
import net.d2dcloud.api.vo.GrouponRulesItemVO;
import net.d2dcloud.api.vo.GrouponRulesSearchVO;

import org.springframework.web.bind.annotation.*;
import org.springframework.cloud.openfeign.FeignClient;

import javax.validation.Valid;

import java.util.List;

/**
 *
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@FeignClient("lm-service")
public interface GrouponRulesFeignClient  {

  /**
   * 新增GrouponRules
   *
   * @param grouponRulesVO
   * @return
   */
  @PostMapping("/grouponRules")
  LmResult<Boolean> add(@Valid @RequestBody GrouponRulesVO grouponRulesVO);

  /**
   * 修改GrouponRules
   *
   * @param grouponRulesUpdateVO
   * @return
   */
  @PutMapping("/grouponRules")
  Resp<Boolean> update(@Valid @RequestBody GrouponRulesUpdateVO grouponRulesUpdateVO) ;

  /**
   * 删除GrouponRules
   *
   * @param id
   * @return
   */
  @DeleteMapping("/grouponRules/{id}")
  Resp<Boolean> delete(@PathVariable("id")String id) ;

  /**
   * 获取GrouponRules详情
   *
   * @param id
   * @return
   */
  @GetMapping("/grouponRules/{id}")
  Resp<GrouponRulesDetailVO> get(@PathVariable("id")String id) ;

  /**
   * 获取GrouponRules详情列表
   *
   * @param ids
   * @return
   */
  @GetMapping("/grouponRules/list")
  Resp<List<GrouponRulesItemVO>> getList(List<String> ids) ;

  /**
   * 获取GrouponRules分页列表
   *
   * @param grouponRulesSearchVO
   * @return
   */
  @GetMapping("/grouponRules/page")
  Resp<PgRet<GrouponRulesItemVO>> getPage(GrouponRulesSearchVO grouponRulesSearchVO);

}