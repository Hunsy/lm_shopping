/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.api.vo;

import lombok.Data;

import java.util.Date;

import java.io.Serializable;

/**
 *  
 *
 * @author codeGen_1.0.0
 */
@Data
public class SystemItemVO implements Serializable {

	//columns START
	/**
	 * 
	 */
	private String id;
	/**
	 * 系统配置名
	 */
	private String keyName;
	/**
	 * 系统配置值
	 */
	private String keyValue;
	/**
	 * 创建时间
	 */
	private Date addTime;
	/**
	 * 更新时间
	 */
	private Date updateTime;
	/**
	 * 逻辑删除
	 */
	private Boolean deleted;
	//columns END
}


