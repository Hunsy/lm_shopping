/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.api.vo;

import lombok.Data;

import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.io.Serializable;

/**
 *  
 *
 * @author codeGen_1.0.0
 */
@Data
public class StorageUpdateVO implements Serializable {

	//columns START
	@NotNull
	private String id;
	/**
	 * 文件的唯一索引
	 */
	@Size(max = 63)
	private String key;
	/**
	 * 文件名
	 */
	@Size(max = 255)
	private String name;
	/**
	 * 文件类型
	 */
	@Size(max = 20)
	private String type;
	/**
	 * 文件大小
	 */
	private Integer size;
	/**
	 * 文件访问链接
	 */
	@Size(max = 255)
	private String url;
	/**
	 * 创建时间
	 */
	private Date addTime;
	/**
	 * 更新时间
	 */
	private Date updateTime;
	/**
	 * 逻辑删除
	 */
	private Boolean deleted;
	//columns END
}


