/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.api.client;

import net.d2dcloud.api.resp.LmPgRet;
import net.d2dcloud.api.resp.LmResult;

import net.d2dcloud.api.vo.LogVO;
import net.d2dcloud.api.vo.LogDetailVO;
import net.d2dcloud.api.vo.LogUpdateVO;
import net.d2dcloud.api.vo.LogItemVO;
import net.d2dcloud.api.vo.LogSearchVO;

import org.springframework.web.bind.annotation.*;
import org.springframework.cloud.openfeign.FeignClient;

import javax.validation.Valid;

import java.util.List;

/**
 *
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@FeignClient("lm-service")
public interface LogFeignClient  {

  /**
   * 新增Log
   *
   * @param logVO
   * @return
   */
  @PostMapping("/log")
  LmResult<Boolean> add(@Valid @RequestBody LogVO logVO);

  /**
   * 修改Log
   *
   * @param logUpdateVO
   * @return
   */
  @PutMapping("/log")
  Resp<Boolean> update(@Valid @RequestBody LogUpdateVO logUpdateVO) ;

  /**
   * 删除Log
   *
   * @param id
   * @return
   */
  @DeleteMapping("/log/{id}")
  Resp<Boolean> delete(@PathVariable("id")String id) ;

  /**
   * 获取Log详情
   *
   * @param id
   * @return
   */
  @GetMapping("/log/{id}")
  Resp<LogDetailVO> get(@PathVariable("id")String id) ;

  /**
   * 获取Log详情列表
   *
   * @param ids
   * @return
   */
  @GetMapping("/log/list")
  Resp<List<LogItemVO>> getList(List<String> ids) ;

  /**
   * 获取Log分页列表
   *
   * @param logSearchVO
   * @return
   */
  @GetMapping("/log/page")
  Resp<PgRet<LogItemVO>> getPage(LogSearchVO logSearchVO);

}