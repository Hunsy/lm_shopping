/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.api.vo;

import lombok.Data;

import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.io.Serializable;

/**
 *  
 *
 * @author codeGen_1.0.0
 */
@Data
public class NoticeUpdateVO implements Serializable {

	//columns START
	@NotNull
	private String id;
	/**
	 * 通知标题
	 */
	@Size(max = 63)
	private String title;
	/**
	 * 通知内容
	 */
	@Size(max = 511)
	private String content;
	/**
	 * 创建通知的管理员ID，如果是系统内置通知则是0.
	 */
	private Integer adminId;
	/**
	 * 创建时间
	 */
	private Date addTime;
	/**
	 * 更新时间
	 */
	private Date updateTime;
	/**
	 * 逻辑删除
	 */
	private Boolean deleted;
	//columns END
}


