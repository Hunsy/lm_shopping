/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.api.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

import net.d2dcloud.resp.AdminPgInVo;

/**
 *  
 *
 * @author codeGen_1.0.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class AdminSearchVO extends AdminPgInVo {

	//columns START
	/**
	 * 
	 */
	private String id;
	/**
	 * 管理员名称
	 */
	private String username;
	/**
	 * 管理员密码
	 */
	private String password;
	/**
	 * 最近一次登录IP地址
	 */
	private String lastLoginIp;
	/**
	 * 最近一次登录时间
	 */
	private Date lastLoginTime;
	/**
	 * 头像图片
	 */
	private String avatar;
	/**
	 * 创建时间
	 */
	private Date addTime;
	/**
	 * 更新时间
	 */
	private Date updateTime;
	/**
	 * 逻辑删除
	 */
	private Boolean deleted;
	/**
	 * 角色列表
	 */
	private String roleIds;
	//columns END
}


