/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.api.client;

import net.d2dcloud.api.resp.LmPgRet;
import net.d2dcloud.api.resp.LmResult;

import net.d2dcloud.api.vo.KeywordVO;
import net.d2dcloud.api.vo.KeywordDetailVO;
import net.d2dcloud.api.vo.KeywordUpdateVO;
import net.d2dcloud.api.vo.KeywordItemVO;
import net.d2dcloud.api.vo.KeywordSearchVO;

import org.springframework.web.bind.annotation.*;
import org.springframework.cloud.openfeign.FeignClient;

import javax.validation.Valid;

import java.util.List;

/**
 *
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@FeignClient("lm-service")
public interface KeywordFeignClient  {

  /**
   * 新增Keyword
   *
   * @param keywordVO
   * @return
   */
  @PostMapping("/keyword")
  LmResult<Boolean> add(@Valid @RequestBody KeywordVO keywordVO);

  /**
   * 修改Keyword
   *
   * @param keywordUpdateVO
   * @return
   */
  @PutMapping("/keyword")
  Resp<Boolean> update(@Valid @RequestBody KeywordUpdateVO keywordUpdateVO) ;

  /**
   * 删除Keyword
   *
   * @param id
   * @return
   */
  @DeleteMapping("/keyword/{id}")
  Resp<Boolean> delete(@PathVariable("id")String id) ;

  /**
   * 获取Keyword详情
   *
   * @param id
   * @return
   */
  @GetMapping("/keyword/{id}")
  Resp<KeywordDetailVO> get(@PathVariable("id")String id) ;

  /**
   * 获取Keyword详情列表
   *
   * @param ids
   * @return
   */
  @GetMapping("/keyword/list")
  Resp<List<KeywordItemVO>> getList(List<String> ids) ;

  /**
   * 获取Keyword分页列表
   *
   * @param keywordSearchVO
   * @return
   */
  @GetMapping("/keyword/page")
  Resp<PgRet<KeywordItemVO>> getPage(KeywordSearchVO keywordSearchVO);

}