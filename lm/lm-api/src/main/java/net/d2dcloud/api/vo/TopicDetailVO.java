/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.api.vo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

import java.io.Serializable;


/**
 *  
 *
 * @author codeGen_1.0.0
 */
@Data
public class TopicDetailVO implements Serializable {

	//columns START
	/**
	 * 
	 */
	private String id;
	/**
	 * 专题标题
	 */
	private String title;
	/**
	 * 专题子标题
	 */
	private String subtitle;
	/**
	 * 专题内容，富文本格式
	 */
	private String content;
	/**
	 * 专题相关商品最低价
	 */
	private BigDecimal price;
	/**
	 * 专题阅读量
	 */
	private String readCount;
	/**
	 * 专题图片
	 */
	private String picUrl;
	/**
	 * 排序
	 */
	private Integer sortOrder;
	/**
	 * 专题相关商品，采用JSON数组格式
	 */
	private String goods;
	/**
	 * 创建时间
	 */
	private Date addTime;
	/**
	 * 更新时间
	 */
	private Date updateTime;
	/**
	 * 逻辑删除
	 */
	private Boolean deleted;
	//columns END
}


