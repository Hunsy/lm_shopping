/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.api.vo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.io.Serializable;

/**
 *
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@Data
public class AftersaleVO implements Serializable {

	//columns START
	/**
	 * 售后编号
	 */
	@NotNull
	@Size(max = 63)
	private String aftersaleSn;
	/**
	 * 订单ID
	 */
	private Integer orderId;
	/**
	 * 用户ID
	 */
	private Integer userId;
	/**
	 * 售后类型，0是未收货退款，1是已收货（无需退货）退款，2用户退货退款
	 */
	@NotNull
	private Integer type;
	/**
	 * 退款原因
	 */
	@NotNull
	@Size(max = 31)
	private String reason;
	/**
	 * 退款金额
	 */
	@NotNull
	private BigDecimal amount;
	/**
	 * 退款凭证图片链接数组
	 */
	@NotNull
	@Size(max = 1023)
	private String pictures;
	/**
	 * 退款说明
	 */
	@NotNull
	@Size(max = 511)
	private String comment;
	/**
	 * 售后状态，0是可申请，1是用户已申请，2是管理员审核通过，3是管理员退款成功，4是管理员审核拒绝，5是用户已取消
	 */
	@NotNull
	private Integer status;
	/**
	 * 管理员操作时间
	 */
	@NotNull
	private Date handleTime;
	/**
	 * 添加时间
	 */
	@NotNull
	private Date addTime;
	/**
	 * 更新时间
	 */
	@NotNull
	private Date updateTime;
	/**
	 * 售后编号
	 */
	@NotNull
	private Boolean deleted;
	//columns END
}


