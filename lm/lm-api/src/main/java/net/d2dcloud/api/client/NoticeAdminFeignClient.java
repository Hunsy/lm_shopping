/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.api.client;

import net.d2dcloud.api.resp.LmPgRet;
import net.d2dcloud.api.resp.LmResult;

import net.d2dcloud.api.vo.NoticeAdminVO;
import net.d2dcloud.api.vo.NoticeAdminDetailVO;
import net.d2dcloud.api.vo.NoticeAdminUpdateVO;
import net.d2dcloud.api.vo.NoticeAdminItemVO;
import net.d2dcloud.api.vo.NoticeAdminSearchVO;

import org.springframework.web.bind.annotation.*;
import org.springframework.cloud.openfeign.FeignClient;

import javax.validation.Valid;

import java.util.List;

/**
 *
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@FeignClient("lm-service")
public interface NoticeAdminFeignClient  {

  /**
   * 新增NoticeAdmin
   *
   * @param noticeAdminVO
   * @return
   */
  @PostMapping("/noticeAdmin")
  LmResult<Boolean> add(@Valid @RequestBody NoticeAdminVO noticeAdminVO);

  /**
   * 修改NoticeAdmin
   *
   * @param noticeAdminUpdateVO
   * @return
   */
  @PutMapping("/noticeAdmin")
  Resp<Boolean> update(@Valid @RequestBody NoticeAdminUpdateVO noticeAdminUpdateVO) ;

  /**
   * 删除NoticeAdmin
   *
   * @param id
   * @return
   */
  @DeleteMapping("/noticeAdmin/{id}")
  Resp<Boolean> delete(@PathVariable("id")String id) ;

  /**
   * 获取NoticeAdmin详情
   *
   * @param id
   * @return
   */
  @GetMapping("/noticeAdmin/{id}")
  Resp<NoticeAdminDetailVO> get(@PathVariable("id")String id) ;

  /**
   * 获取NoticeAdmin详情列表
   *
   * @param ids
   * @return
   */
  @GetMapping("/noticeAdmin/list")
  Resp<List<NoticeAdminItemVO>> getList(List<String> ids) ;

  /**
   * 获取NoticeAdmin分页列表
   *
   * @param noticeAdminSearchVO
   * @return
   */
  @GetMapping("/noticeAdmin/page")
  Resp<PgRet<NoticeAdminItemVO>> getPage(NoticeAdminSearchVO noticeAdminSearchVO);

}