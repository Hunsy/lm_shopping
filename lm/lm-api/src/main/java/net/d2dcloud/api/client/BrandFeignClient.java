/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.api.client;

import net.d2dcloud.api.vo.*;
import net.d2dcloud.core.resp.PgRet;
import net.d2dcloud.core.resp.Resp;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@FeignClient("lm-service")
public interface BrandFeignClient {

  /**
   * 新增Brand
   *
   * @param brandVO
   * @return
   */
  @PostMapping("/brand")
  Resp<Boolean> add(@Valid @RequestBody BrandVO brandVO);

  /**
   * 修改Brand
   *
   * @param brandUpdateVO
   * @return
   */
  @PutMapping("/brand")
  Resp<Boolean> update(@Valid @RequestBody BrandUpdateVO brandUpdateVO);

  /**
   * 删除Brand
   *
   * @param id
   * @return
   */
  @DeleteMapping("/brand/{id}")
  Resp<Boolean> delete(@PathVariable("id") String id);

  /**
   * 获取Brand详情
   *
   * @param id
   * @return
   */
  @GetMapping("/brand/{id}")
  Resp<BrandDetailVO> get(@PathVariable("id") String id);

  /**
   * 获取Brand详情列表
   *
   * @param ids
   * @return
   */
  @GetMapping("/brand/list")
  Resp<List<BrandItemVO>> getList(List<String> ids);

  /**
   * 获取Brand分页列表
   *
   * @param brandSearchVO
   * @return
   */
  @GetMapping("/brand/page")
  Resp<PgRet<BrandItemVO>> getPage(BrandSearchVO brandSearchVO);

}