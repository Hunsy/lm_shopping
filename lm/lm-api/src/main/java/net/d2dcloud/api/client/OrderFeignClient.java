/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.api.client;

import net.d2dcloud.api.resp.LmPgRet;
import net.d2dcloud.api.resp.LmResult;

import net.d2dcloud.api.vo.OrderVO;
import net.d2dcloud.api.vo.OrderDetailVO;
import net.d2dcloud.api.vo.OrderUpdateVO;
import net.d2dcloud.api.vo.OrderItemVO;
import net.d2dcloud.api.vo.OrderSearchVO;

import org.springframework.web.bind.annotation.*;
import org.springframework.cloud.openfeign.FeignClient;

import javax.validation.Valid;

import java.util.List;

/**
 *
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@FeignClient("lm-service")
public interface OrderFeignClient  {

  /**
   * 新增Order
   *
   * @param orderVO
   * @return
   */
  @PostMapping("/order")
  LmResult<Boolean> add(@Valid @RequestBody OrderVO orderVO);

  /**
   * 修改Order
   *
   * @param orderUpdateVO
   * @return
   */
  @PutMapping("/order")
  Resp<Boolean> update(@Valid @RequestBody OrderUpdateVO orderUpdateVO) ;

  /**
   * 删除Order
   *
   * @param id
   * @return
   */
  @DeleteMapping("/order/{id}")
  Resp<Boolean> delete(@PathVariable("id")String id) ;

  /**
   * 获取Order详情
   *
   * @param id
   * @return
   */
  @GetMapping("/order/{id}")
  Resp<OrderDetailVO> get(@PathVariable("id")String id) ;

  /**
   * 获取Order详情列表
   *
   * @param ids
   * @return
   */
  @GetMapping("/order/list")
  Resp<List<OrderItemVO>> getList(List<String> ids) ;

  /**
   * 获取Order分页列表
   *
   * @param orderSearchVO
   * @return
   */
  @GetMapping("/order/page")
  Resp<PgRet<OrderItemVO>> getPage(OrderSearchVO orderSearchVO);

}