/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.api.client;

import net.d2dcloud.api.resp.LmPgRet;
import net.d2dcloud.api.resp.LmResult;

import net.d2dcloud.api.vo.FootprintVO;
import net.d2dcloud.api.vo.FootprintDetailVO;
import net.d2dcloud.api.vo.FootprintUpdateVO;
import net.d2dcloud.api.vo.FootprintItemVO;
import net.d2dcloud.api.vo.FootprintSearchVO;

import org.springframework.web.bind.annotation.*;
import org.springframework.cloud.openfeign.FeignClient;

import javax.validation.Valid;

import java.util.List;

/**
 *
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@FeignClient("lm-service")
public interface FootprintFeignClient  {

  /**
   * 新增Footprint
   *
   * @param footprintVO
   * @return
   */
  @PostMapping("/footprint")
  LmResult<Boolean> add(@Valid @RequestBody FootprintVO footprintVO);

  /**
   * 修改Footprint
   *
   * @param footprintUpdateVO
   * @return
   */
  @PutMapping("/footprint")
  Resp<Boolean> update(@Valid @RequestBody FootprintUpdateVO footprintUpdateVO) ;

  /**
   * 删除Footprint
   *
   * @param id
   * @return
   */
  @DeleteMapping("/footprint/{id}")
  Resp<Boolean> delete(@PathVariable("id")String id) ;

  /**
   * 获取Footprint详情
   *
   * @param id
   * @return
   */
  @GetMapping("/footprint/{id}")
  Resp<FootprintDetailVO> get(@PathVariable("id")String id) ;

  /**
   * 获取Footprint详情列表
   *
   * @param ids
   * @return
   */
  @GetMapping("/footprint/list")
  Resp<List<FootprintItemVO>> getList(List<String> ids) ;

  /**
   * 获取Footprint分页列表
   *
   * @param footprintSearchVO
   * @return
   */
  @GetMapping("/footprint/page")
  Resp<PgRet<FootprintItemVO>> getPage(FootprintSearchVO footprintSearchVO);

}