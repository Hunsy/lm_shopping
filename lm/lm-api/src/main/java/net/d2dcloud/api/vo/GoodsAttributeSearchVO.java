/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.api.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

import net.d2dcloud.resp.GoodsAttributePgInVo;

/**
 *  
 *
 * @author codeGen_1.0.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class GoodsAttributeSearchVO extends GoodsAttributePgInVo {

	//columns START
	/**
	 * 
	 */
	private String id;
	/**
	 * 商品表的商品ID
	 */
	private Integer goodsId;
	/**
	 * 商品参数名称
	 */
	private String attribute;
	/**
	 * 商品参数值
	 */
	private String value;
	/**
	 * 创建时间
	 */
	private Date addTime;
	/**
	 * 更新时间
	 */
	private Date updateTime;
	/**
	 * 逻辑删除
	 */
	private Boolean deleted;
	//columns END
}


