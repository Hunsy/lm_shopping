/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.api.client;

import net.d2dcloud.api.resp.LmPgRet;
import net.d2dcloud.api.resp.LmResult;

import net.d2dcloud.api.vo.SearchHistoryVO;
import net.d2dcloud.api.vo.SearchHistoryDetailVO;
import net.d2dcloud.api.vo.SearchHistoryUpdateVO;
import net.d2dcloud.api.vo.SearchHistoryItemVO;
import net.d2dcloud.api.vo.SearchHistorySearchVO;

import org.springframework.web.bind.annotation.*;
import org.springframework.cloud.openfeign.FeignClient;

import javax.validation.Valid;

import java.util.List;

/**
 *
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@FeignClient("lm-service")
public interface SearchHistoryFeignClient  {

  /**
   * 新增SearchHistory
   *
   * @param searchHistoryVO
   * @return
   */
  @PostMapping("/searchHistory")
  LmResult<Boolean> add(@Valid @RequestBody SearchHistoryVO searchHistoryVO);

  /**
   * 修改SearchHistory
   *
   * @param searchHistoryUpdateVO
   * @return
   */
  @PutMapping("/searchHistory")
  Resp<Boolean> update(@Valid @RequestBody SearchHistoryUpdateVO searchHistoryUpdateVO) ;

  /**
   * 删除SearchHistory
   *
   * @param id
   * @return
   */
  @DeleteMapping("/searchHistory/{id}")
  Resp<Boolean> delete(@PathVariable("id")String id) ;

  /**
   * 获取SearchHistory详情
   *
   * @param id
   * @return
   */
  @GetMapping("/searchHistory/{id}")
  Resp<SearchHistoryDetailVO> get(@PathVariable("id")String id) ;

  /**
   * 获取SearchHistory详情列表
   *
   * @param ids
   * @return
   */
  @GetMapping("/searchHistory/list")
  Resp<List<SearchHistoryItemVO>> getList(List<String> ids) ;

  /**
   * 获取SearchHistory分页列表
   *
   * @param searchHistorySearchVO
   * @return
   */
  @GetMapping("/searchHistory/page")
  Resp<PgRet<SearchHistoryItemVO>> getPage(SearchHistorySearchVO searchHistorySearchVO);

}