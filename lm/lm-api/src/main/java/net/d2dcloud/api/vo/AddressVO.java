/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.api.vo;

import lombok.Data;

import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.io.Serializable;

/**
 *
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@Data
public class AddressVO implements Serializable {

	//columns START
	/**
	 * 收货人名称
	 */
	@Size(max = 63)
	private String name;
	/**
	 * 用户表的用户ID
	 */
	private Integer userId;
	/**
	 * 行政区域表的省ID
	 */
	@Size(max = 63)
	private String province;
	/**
	 * 行政区域表的市ID
	 */
	@Size(max = 63)
	private String city;
	/**
	 * 行政区域表的区县ID
	 */
	@Size(max = 63)
	private String county;
	/**
	 * 详细收货地址
	 */
	@Size(max = 127)
	private String addressDetail;
	/**
	 * 地区编码
	 */
	@NotNull
	@Size(max = 6)
	private String areaCode;
	/**
	 * 邮政编码
	 */
	@NotNull
	@Size(max = 6)
	private String postalCode;
	/**
	 * 手机号码
	 */
	@Size(max = 20)
	private String tel;
	/**
	 * 是否默认地址
	 */
	private Boolean isDefault;
	/**
	 * 创建时间
	 */
	@NotNull
	private Date addTime;
	/**
	 * 更新时间
	 */
	@NotNull
	private Date updateTime;
	/**
	 * 逻辑删除
	 */
	@NotNull
	private Boolean deleted;
	//columns END
}


