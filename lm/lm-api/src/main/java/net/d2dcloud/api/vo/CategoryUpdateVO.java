/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.api.vo;

import lombok.Data;

import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.io.Serializable;

/**
 *  
 *
 * @author codeGen_1.0.0
 */
@Data
public class CategoryUpdateVO implements Serializable {

	//columns START
	@NotNull
	private String id;
	/**
	 * 类目名称
	 */
	@Size(max = 63)
	private String name;
	/**
	 * 类目关键字，以JSON数组格式
	 */
	@Size(max = 1023)
	private String keywords;
	/**
	 * 类目广告语介绍
	 */
	@Size(max = 255)
	private String desc;
	/**
	 * 父类目ID
	 */
	private Integer pid;
	/**
	 * 类目图标
	 */
	@Size(max = 255)
	private String iconUrl;
	/**
	 * 类目图片
	 */
	@Size(max = 255)
	private String picUrl;
	/**
	 * 
	 */
	@Size(max = 255)
	private String level;
	/**
	 * 排序
	 */
	private Integer sortOrder;
	/**
	 * 创建时间
	 */
	private Date addTime;
	/**
	 * 更新时间
	 */
	private Date updateTime;
	/**
	 * 逻辑删除
	 */
	private Boolean deleted;
	//columns END
}


