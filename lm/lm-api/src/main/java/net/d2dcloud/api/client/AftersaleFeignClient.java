/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.api.client;

import net.d2dcloud.api.vo.*;
import net.d2dcloud.core.resp.PgRet;
import net.d2dcloud.core.resp.Resp;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@FeignClient("lm-service")
public interface AftersaleFeignClient {

  /**
   * 新增Aftersale
   *
   * @param aftersaleVO
   * @return
   */
  @PostMapping("/aftersale")
  Resp<Boolean> add(@Valid @RequestBody AftersaleVO aftersaleVO);

  /**
   * 修改Aftersale
   *
   * @param aftersaleUpdateVO
   * @return
   */
  @PutMapping("/aftersale")
  Resp<Boolean> update(@Valid @RequestBody AftersaleUpdateVO aftersaleUpdateVO);

  /**
   * 删除Aftersale
   *
   * @param id
   * @return
   */
  @DeleteMapping("/aftersale/{id}")
  Resp<Boolean> delete(@PathVariable("id") String id);

  /**
   * 获取Aftersale详情
   *
   * @param id
   * @return
   */
  @GetMapping("/aftersale/{id}")
  Resp<AftersaleDetailVO> get(@PathVariable("id") String id);

  /**
   * 获取Aftersale详情列表
   *
   * @param ids
   * @return
   */
  @GetMapping("/aftersale/list")
  Resp<List<AftersaleItemVO>> getList(List<String> ids);

  /**
   * 获取Aftersale分页列表
   *
   * @param aftersaleSearchVO
   * @return
   */
  @GetMapping("/aftersale/page")
  Resp<PgRet<AftersaleItemVO>> getPage(AftersaleSearchVO aftersaleSearchVO);

}