/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.api.client;

import net.d2dcloud.api.resp.LmPgRet;
import net.d2dcloud.api.resp.LmResult;

import net.d2dcloud.api.vo.NoticeVO;
import net.d2dcloud.api.vo.NoticeDetailVO;
import net.d2dcloud.api.vo.NoticeUpdateVO;
import net.d2dcloud.api.vo.NoticeItemVO;
import net.d2dcloud.api.vo.NoticeSearchVO;

import org.springframework.web.bind.annotation.*;
import org.springframework.cloud.openfeign.FeignClient;

import javax.validation.Valid;

import java.util.List;

/**
 *
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@FeignClient("lm-service")
public interface NoticeFeignClient  {

  /**
   * 新增Notice
   *
   * @param noticeVO
   * @return
   */
  @PostMapping("/notice")
  LmResult<Boolean> add(@Valid @RequestBody NoticeVO noticeVO);

  /**
   * 修改Notice
   *
   * @param noticeUpdateVO
   * @return
   */
  @PutMapping("/notice")
  Resp<Boolean> update(@Valid @RequestBody NoticeUpdateVO noticeUpdateVO) ;

  /**
   * 删除Notice
   *
   * @param id
   * @return
   */
  @DeleteMapping("/notice/{id}")
  Resp<Boolean> delete(@PathVariable("id")String id) ;

  /**
   * 获取Notice详情
   *
   * @param id
   * @return
   */
  @GetMapping("/notice/{id}")
  Resp<NoticeDetailVO> get(@PathVariable("id")String id) ;

  /**
   * 获取Notice详情列表
   *
   * @param ids
   * @return
   */
  @GetMapping("/notice/list")
  Resp<List<NoticeItemVO>> getList(List<String> ids) ;

  /**
   * 获取Notice分页列表
   *
   * @param noticeSearchVO
   * @return
   */
  @GetMapping("/notice/page")
  Resp<PgRet<NoticeItemVO>> getPage(NoticeSearchVO noticeSearchVO);

}