/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.api.client;

import net.d2dcloud.api.resp.LmPgRet;
import net.d2dcloud.api.resp.LmResult;

import net.d2dcloud.api.vo.PermissionVO;
import net.d2dcloud.api.vo.PermissionDetailVO;
import net.d2dcloud.api.vo.PermissionUpdateVO;
import net.d2dcloud.api.vo.PermissionItemVO;
import net.d2dcloud.api.vo.PermissionSearchVO;

import org.springframework.web.bind.annotation.*;
import org.springframework.cloud.openfeign.FeignClient;

import javax.validation.Valid;

import java.util.List;

/**
 *
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@FeignClient("lm-service")
public interface PermissionFeignClient  {

  /**
   * 新增Permission
   *
   * @param permissionVO
   * @return
   */
  @PostMapping("/permission")
  LmResult<Boolean> add(@Valid @RequestBody PermissionVO permissionVO);

  /**
   * 修改Permission
   *
   * @param permissionUpdateVO
   * @return
   */
  @PutMapping("/permission")
  Resp<Boolean> update(@Valid @RequestBody PermissionUpdateVO permissionUpdateVO) ;

  /**
   * 删除Permission
   *
   * @param id
   * @return
   */
  @DeleteMapping("/permission/{id}")
  Resp<Boolean> delete(@PathVariable("id")String id) ;

  /**
   * 获取Permission详情
   *
   * @param id
   * @return
   */
  @GetMapping("/permission/{id}")
  Resp<PermissionDetailVO> get(@PathVariable("id")String id) ;

  /**
   * 获取Permission详情列表
   *
   * @param ids
   * @return
   */
  @GetMapping("/permission/list")
  Resp<List<PermissionItemVO>> getList(List<String> ids) ;

  /**
   * 获取Permission分页列表
   *
   * @param permissionSearchVO
   * @return
   */
  @GetMapping("/permission/page")
  Resp<PgRet<PermissionItemVO>> getPage(PermissionSearchVO permissionSearchVO);

}