/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.api.client;

import net.d2dcloud.api.resp.LmPgRet;
import net.d2dcloud.api.resp.LmResult;

import net.d2dcloud.api.vo.RegionVO;
import net.d2dcloud.api.vo.RegionDetailVO;
import net.d2dcloud.api.vo.RegionUpdateVO;
import net.d2dcloud.api.vo.RegionItemVO;
import net.d2dcloud.api.vo.RegionSearchVO;

import org.springframework.web.bind.annotation.*;
import org.springframework.cloud.openfeign.FeignClient;

import javax.validation.Valid;

import java.util.List;

/**
 *
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@FeignClient("lm-service")
public interface RegionFeignClient  {

  /**
   * 新增Region
   *
   * @param regionVO
   * @return
   */
  @PostMapping("/region")
  LmResult<Boolean> add(@Valid @RequestBody RegionVO regionVO);

  /**
   * 修改Region
   *
   * @param regionUpdateVO
   * @return
   */
  @PutMapping("/region")
  Resp<Boolean> update(@Valid @RequestBody RegionUpdateVO regionUpdateVO) ;

  /**
   * 删除Region
   *
   * @param id
   * @return
   */
  @DeleteMapping("/region/{id}")
  Resp<Boolean> delete(@PathVariable("id")String id) ;

  /**
   * 获取Region详情
   *
   * @param id
   * @return
   */
  @GetMapping("/region/{id}")
  Resp<RegionDetailVO> get(@PathVariable("id")String id) ;

  /**
   * 获取Region详情列表
   *
   * @param ids
   * @return
   */
  @GetMapping("/region/list")
  Resp<List<RegionItemVO>> getList(List<String> ids) ;

  /**
   * 获取Region分页列表
   *
   * @param regionSearchVO
   * @return
   */
  @GetMapping("/region/page")
  Resp<PgRet<RegionItemVO>> getPage(RegionSearchVO regionSearchVO);

}