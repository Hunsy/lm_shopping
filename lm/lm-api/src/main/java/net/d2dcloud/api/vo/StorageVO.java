/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.api.vo;

import lombok.Data;

import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.io.Serializable;

/**
 *
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@Data
public class StorageVO implements Serializable {

	//columns START
	/**
	 * 文件的唯一索引
	 */
	@Size(max = 63)
	private String key;
	/**
	 * 文件名
	 */
	@Size(max = 255)
	private String name;
	/**
	 * 文件类型
	 */
	@Size(max = 20)
	private String type;
	/**
	 * 文件大小
	 */
	private Integer size;
	/**
	 * 文件访问链接
	 */
	@NotNull
	@Size(max = 255)
	private String url;
	/**
	 * 创建时间
	 */
	@NotNull
	private Date addTime;
	/**
	 * 更新时间
	 */
	@NotNull
	private Date updateTime;
	/**
	 * 逻辑删除
	 */
	@NotNull
	private Boolean deleted;
	//columns END
}


