/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.api.client;

import net.d2dcloud.api.resp.LmPgRet;
import net.d2dcloud.api.resp.LmResult;

import net.d2dcloud.api.vo.GoodsVO;
import net.d2dcloud.api.vo.GoodsDetailVO;
import net.d2dcloud.api.vo.GoodsUpdateVO;
import net.d2dcloud.api.vo.GoodsItemVO;
import net.d2dcloud.api.vo.GoodsSearchVO;

import org.springframework.web.bind.annotation.*;
import org.springframework.cloud.openfeign.FeignClient;

import javax.validation.Valid;

import java.util.List;

/**
 *
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@FeignClient("lm-service")
public interface GoodsFeignClient  {

  /**
   * 新增Goods
   *
   * @param goodsVO
   * @return
   */
  @PostMapping("/goods")
  LmResult<Boolean> add(@Valid @RequestBody GoodsVO goodsVO);

  /**
   * 修改Goods
   *
   * @param goodsUpdateVO
   * @return
   */
  @PutMapping("/goods")
  Resp<Boolean> update(@Valid @RequestBody GoodsUpdateVO goodsUpdateVO) ;

  /**
   * 删除Goods
   *
   * @param id
   * @return
   */
  @DeleteMapping("/goods/{id}")
  Resp<Boolean> delete(@PathVariable("id")String id) ;

  /**
   * 获取Goods详情
   *
   * @param id
   * @return
   */
  @GetMapping("/goods/{id}")
  Resp<GoodsDetailVO> get(@PathVariable("id")String id) ;

  /**
   * 获取Goods详情列表
   *
   * @param ids
   * @return
   */
  @GetMapping("/goods/list")
  Resp<List<GoodsItemVO>> getList(List<String> ids) ;

  /**
   * 获取Goods分页列表
   *
   * @param goodsSearchVO
   * @return
   */
  @GetMapping("/goods/page")
  Resp<PgRet<GoodsItemVO>> getPage(GoodsSearchVO goodsSearchVO);

}