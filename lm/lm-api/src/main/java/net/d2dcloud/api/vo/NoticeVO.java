/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.api.vo;

import lombok.Data;

import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.io.Serializable;

/**
 *
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@Data
public class NoticeVO implements Serializable {

	//columns START
	/**
	 * 通知标题
	 */
	@NotNull
	@Size(max = 63)
	private String title;
	/**
	 * 通知内容
	 */
	@NotNull
	@Size(max = 511)
	private String content;
	/**
	 * 创建通知的管理员ID，如果是系统内置通知则是0.
	 */
	@NotNull
	private Integer adminId;
	/**
	 * 创建时间
	 */
	@NotNull
	private Date addTime;
	/**
	 * 更新时间
	 */
	@NotNull
	private Date updateTime;
	/**
	 * 逻辑删除
	 */
	@NotNull
	private Boolean deleted;
	//columns END
}


