/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.api.vo;

import lombok.Data;

import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.io.Serializable;

/**
 *
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@Data
public class GoodsAttributeVO implements Serializable {

	//columns START
	/**
	 * 商品表的商品ID
	 */
	private Integer goodsId;
	/**
	 * 商品参数名称
	 */
	@Size(max = 255)
	private String attribute;
	/**
	 * 商品参数值
	 */
	@Size(max = 255)
	private String value;
	/**
	 * 创建时间
	 */
	@NotNull
	private Date addTime;
	/**
	 * 更新时间
	 */
	@NotNull
	private Date updateTime;
	/**
	 * 逻辑删除
	 */
	@NotNull
	private Boolean deleted;
	//columns END
}


