/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.api.vo;

import lombok.Data;

import java.util.Date;

import java.io.Serializable;


/**
 *  
 *
 * @author codeGen_1.0.0
 */
@Data
public class PermissionDetailVO implements Serializable {

	//columns START
	/**
	 * 
	 */
	private String id;
	/**
	 * 角色ID
	 */
	private Integer roleId;
	/**
	 * 权限
	 */
	private String permission;
	/**
	 * 创建时间
	 */
	private Date addTime;
	/**
	 * 更新时间
	 */
	private Date updateTime;
	/**
	 * 逻辑删除
	 */
	private Boolean deleted;
	//columns END
}


