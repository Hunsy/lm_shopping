/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.api.vo;

import lombok.Data;

import java.util.Date;

import java.io.Serializable;

/**
 *  
 *
 * @author codeGen_1.0.0
 */
@Data
public class NoticeAdminItemVO implements Serializable {

	//columns START
	/**
	 * 
	 */
	private String id;
	/**
	 * 通知ID
	 */
	private Integer noticeId;
	/**
	 * 通知标题
	 */
	private String noticeTitle;
	/**
	 * 接收通知的管理员ID
	 */
	private Integer adminId;
	/**
	 * 阅读时间，如果是NULL则是未读状态
	 */
	private Date readTime;
	/**
	 * 创建时间
	 */
	private Date addTime;
	/**
	 * 更新时间
	 */
	private Date updateTime;
	/**
	 * 逻辑删除
	 */
	private Boolean deleted;
	//columns END
}


