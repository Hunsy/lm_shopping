/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2020
 */
package net.d2dcloud.api.client;

import net.d2dcloud.api.resp.LmPgRet;
import net.d2dcloud.api.resp.LmResult;

import net.d2dcloud.api.vo.GoodsSpecificationVO;
import net.d2dcloud.api.vo.GoodsSpecificationDetailVO;
import net.d2dcloud.api.vo.GoodsSpecificationUpdateVO;
import net.d2dcloud.api.vo.GoodsSpecificationItemVO;
import net.d2dcloud.api.vo.GoodsSpecificationSearchVO;

import org.springframework.web.bind.annotation.*;
import org.springframework.cloud.openfeign.FeignClient;

import javax.validation.Valid;

import java.util.List;

/**
 *
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@FeignClient("lm-service")
public interface GoodsSpecificationFeignClient  {

  /**
   * 新增GoodsSpecification
   *
   * @param goodsSpecificationVO
   * @return
   */
  @PostMapping("/goodsSpecification")
  LmResult<Boolean> add(@Valid @RequestBody GoodsSpecificationVO goodsSpecificationVO);

  /**
   * 修改GoodsSpecification
   *
   * @param goodsSpecificationUpdateVO
   * @return
   */
  @PutMapping("/goodsSpecification")
  Resp<Boolean> update(@Valid @RequestBody GoodsSpecificationUpdateVO goodsSpecificationUpdateVO) ;

  /**
   * 删除GoodsSpecification
   *
   * @param id
   * @return
   */
  @DeleteMapping("/goodsSpecification/{id}")
  Resp<Boolean> delete(@PathVariable("id")String id) ;

  /**
   * 获取GoodsSpecification详情
   *
   * @param id
   * @return
   */
  @GetMapping("/goodsSpecification/{id}")
  Resp<GoodsSpecificationDetailVO> get(@PathVariable("id")String id) ;

  /**
   * 获取GoodsSpecification详情列表
   *
   * @param ids
   * @return
   */
  @GetMapping("/goodsSpecification/list")
  Resp<List<GoodsSpecificationItemVO>> getList(List<String> ids) ;

  /**
   * 获取GoodsSpecification分页列表
   *
   * @param goodsSpecificationSearchVO
   * @return
   */
  @GetMapping("/goodsSpecification/page")
  Resp<PgRet<GoodsSpecificationItemVO>> getPage(GoodsSpecificationSearchVO goodsSpecificationSearchVO);

}