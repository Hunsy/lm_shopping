package net.d2dcloud.core.tk.mapper;

import net.d2dcloud.core.tk.entity.BaseEntity;
import tk.mybatis.mapper.common.Mapper;

/**
 * 基础的Mapper
 *
 * @author hunsy
 */
public interface BaseMapper<T extends BaseEntity> extends Mapper<T> {
}
