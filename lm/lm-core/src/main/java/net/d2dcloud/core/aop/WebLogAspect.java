package net.d2dcloud.core.aop;

import com.alibaba.fastjson.JSON;
import net.d2dcloud.core.constants.RequestContext;
import net.d2dcloud.core.ex.BizException;
import net.d2dcloud.core.resp.Resp;
import net.d2dcloud.core.resp.RetCode;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.support.StandardMultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

/**
 * 日志切面
 *
 * @author hunsy
 */
@Aspect
@Component
@Slf4j
public class WebLogAspect extends BaseWebLogAspect {

    private ThreadLocal<Map<String, Object>> info = new ThreadLocal<>();

    @Override
    @Pointcut("within(net.d2dcloud.controller..*)")
    public void webLog() {
    }

}
