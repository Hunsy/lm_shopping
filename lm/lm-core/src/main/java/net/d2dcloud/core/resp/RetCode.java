/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2019
 */
package net.d2dcloud.core.resp;

import lombok.Getter;

/**
 * 返回的code的枚举
 *
 * @author hunsy
 */
@Getter
public enum RetCode {

	SUCCESS(200, "请求成功"),
	AUTH_FAILURE(402, "授权失败"),
	UN_AUTH(401, "鉴权失败"),

	/**
	 * 对象不存在
	 */
	OBJECT_NOT_EXIST(1001, "对象不存在"),
	/**
	 * 关联性操作失败，抛出异常，回滚
	 */
	TRANSACTION_FAIL(1002, "关联操作失败"),

	//用户相关
	USER_NOT_EXIST(100000, "用户不存在"),
	USER_PASSWORD_ERROR(100001, "用户密码错误"),
	USER_EXIST(100002, "用户已经存在"),


	SYSTEM_RESOURCE(999999, "系统资源，不能删除"),

	ILLEGAL_OPERATION(888888, "非法操作"),


	DEVICE_OPEN_FAIL(200100,"开门失败"),
	DEVICE_OFFLINE(200101,"设备离线"),
	FLOW_NOT_EXIST(200102,"客流不存在"),


	SYSTEM_ERROR(500, "系统异常");

	private Integer code;
	private String msg;

	RetCode(Integer code, String msg) {
		this.code = code;
		this.msg = msg;
	}


	public static RetCode getInstance(Integer code) {
		if (code != null) {
			RetCode[] retCodes = RetCode.values();
			for (RetCode retCode : retCodes) {
				if (code.equals(retCode.code)) {
					return retCode;
				}
			}
		}
		return null;
	}
}
