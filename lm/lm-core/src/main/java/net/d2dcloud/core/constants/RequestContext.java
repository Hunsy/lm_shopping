package net.d2dcloud.core.constants;

import java.util.Map;

/**
 * @author liuzw
 */
public class RequestContext {

  private static ThreadLocal<Map<String, Object>> holder = new ThreadLocal<> ();


  /**
   * 移除上下文环境
   */
  public static void remove() {
    holder.remove ();
  }


}
