package net.d2dcloud.core.tk.entity;

import lombok.Data;

import javax.persistence.Id;
import java.io.Serializable;

/**
 * Entity基础类
 *
 * @author hunsy
 */
@Data
public abstract class BaseEntity implements Serializable {
  @Id
  private String id;
}
