/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2019
 */
package net.d2dcloud.core.resp;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 响应bean
 *
 * @author hunsy
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Resp<T> implements Serializable {

    private Integer code;
    private String msg;
    private T data;

}
