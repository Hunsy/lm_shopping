/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2019
 */
package net.d2dcloud.core.resp;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * @author codeGen v0.0.1
 * @version 1.0
 * @since 1.0
 */
@Data
public class PgRet<T> {

  private Long totalNum;
  private Integer totalPage;
  private Integer currentPage;
  private List<T> dataResult = new ArrayList<>();

  public PgRet(Integer pageNum, Long totalRows, Integer totalPage, List<T> objects) {
    this.currentPage = pageNum;
    this.totalNum = totalRows;
    this.totalPage = totalPage;
    if (objects != null) {
      this.dataResult.addAll(objects);
    }
  }
}
