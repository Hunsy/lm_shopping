package net.d2dcloud.core.ex;

import net.d2dcloud.core.resp.RetCode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 业务异常
 *
 * @author hunsy
 */
@Data
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class BizException extends RuntimeException {


    private Integer code;
    private String msg;


    public BizException(RetCode retCode) {
        this.code = retCode.getCode();
        this.msg = retCode.getMsg();
    }

}
