/*
 * Powered By [d2dcloud.net]
 * Since 2016 - 2019
 */
package net.d2dcloud.core.resp;

import lombok.Data;

import java.io.Serializable;

/**
 * 分页查询公共类
 *
 * @author hunsy
 */
@Data
public class PgInVo implements Serializable {

    private Integer pageNo = 1;
    private Integer pageSize = 10;
}
